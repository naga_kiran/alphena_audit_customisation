# Include hook code here
require 'fastercsv'
require 'translator'
require 'paper_trail'
require File.join(File.dirname(__FILE__), "config", "breadcrumbs")
require File.join(File.dirname(__FILE__), "lib", "alphena_audit_customisation")

FedenaPlugin.register = {
  :name=>"alphena_audit_customisation",
  :description=>"Alphena Audit Customisation",
  :auth_file=>"config/alphena_audit_customisation_auth.rb",
  :generic_hook=>{:title=>"add_pay_grade",
    :source=>{:controller=>"employee",:action=>"hr"},
    :destination=>{:controller=>"alphena_employee",:action=>"admission1"}, :description => "alphena_paygrades_expln", :target_id => "box"
  },
  :more_menu=>{:title=>"alphena_report_text",:controller=>"report",:action=>"employee_payroll_reports", :target_id=>"more-parent"}
}

Dir[File.join("#{File.dirname(__FILE__)}/config/locales/*.yml")].each do |locale|
  I18n.load_path.unshift(locale)
end

AlphenaAuditCustomisation.attach_overrides

if RAILS_ENV == 'development'
  ActiveSupport::Dependencies.load_once_paths.\
    reject!{|x| x =~ /^#{Regexp.escape(File.dirname(__FILE__))}/}
end
