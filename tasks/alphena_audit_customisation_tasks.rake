# desc "Explaining what the task does"
# task :alphena_audit_customisation do
#   # Task goes here
# end

namespace :alphena_audit_customisation do
  desc "Install Alphena Audit Module"
  task :install do
    system "rsync --exclude=.svn -ruv vendor/plugins/alphena_audit_customisation/public ."
    system "rsync -ruv --exclude=.svn vendor/plugins/alphena_audit_customisation/db/migrate db"
#     if File.exists?("#{Rails.root}/config/environment.rb")
#      src = File.read("#{Rails.root}/vendor/plugins/alphena_audit_customisation/config/custom_environment.rb")
#      File.open("#{Rails.root}/config/environment.rb", "w") do |file|
#        file.puts(src)
#      end
#   end
  end
end