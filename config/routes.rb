ActionController::Routing::Routes.draw do |map|
  map.set_report_content 'descriptive_reports/set_report_content',:controller => 'dr_remark_settings',:action => 'set_report_content'
  #  map.resources :dr_remark_settings, :collection  => { :download_csv => :get }
  map.download_csv 'dr_remark_settings/:id/download_csv', :controller => 'dr_remark_settings',:action => :download_csv
  map.delete_csv 'dr_remark_settings/:id/delete_csv', :controller => 'dr_remark_settings',:action => :delete_csv
end