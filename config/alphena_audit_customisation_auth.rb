authorization do
  role :audit_manager do

    has_permission_on [:payroll],
      :to => [:add_payroll_value, :assign_payroll_categories, :list_payroll_categories,:save_payroll_categories, :update_alphena_payslip_fields, :update_alphena_dependent_payslips_fields ]

    has_permission_on [:alphena_paygrades],
      :to => [:add_paygrade, :edit_paygrade, :delete_paygrade, :activate_paygrade, :inactivate_paygrade]

    has_permission_on [:finance],
      :to => [:on_revert, :revert_employee_payslip, :on_monthly_revert, :revert_monthly_payslip ]

    has_permission_on [:report],
      :to => [:employee_payroll_category,:employee_payroll_pdf,
      :employee_payroll_reports,:generate_csv_file, :responsibility_csv_file,
      :generate_netsalary_csv_file,:generate_deduction_csv_file,
      :generate_gross_csv_file, :cummulative_report, :cumulative_pdf,
      :cumulative_csv_file,:payrolls_details_csv_file,:alphena_employee_csv_list,:individual_deduction_csv_file,
      :after_employee_created,:monthly_wise_payslip, :monthly_wise_payslip_pdf, :monthly_wise_payslip_csv_file ]

    has_permission_on [:alphena_audits],
      :to => [:alphena_audit_log,:update_module_event,:audit_link,:alphena_activity,:alphena_user_audit,:update_user_search,:update_batch]
    has_permission_on [:costcodes],
       :to => [:index,:new,:create,:show,:edit,:destroy]
    has_permission_on [:employee],
    :to => [:bank_details, :update_bank_details, :generate_csv_file,:department_wise_monthly_payslip,:department_wise_monthly_payslip_csv,:department_wise_monthly_payslip,:gifmis_mandate_report,:update_gifmis_mandate_report,:gifis_csv_file,:alphena_employee_wise_payslip,:alphena_employee_wise_payslip_pdf]

  end

  role :admin do
    includes :audit_manager
  end
end


