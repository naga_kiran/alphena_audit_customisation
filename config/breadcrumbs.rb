Gretel::Crumbs.layout do

########################################
  #alphena-report module
  ########################################

  crumb :reports_employee_payroll_reports do
    link I18n.t('employee_payroll_details'), {:controller=>"report", :action=>"employee_payroll_reports"}
  end

  crumb :reports_employee_payroll_category do
    link I18n.t('payroll_details'), {:controller=>"report", :action=>"employee_payroll_category"}
    parent :reports_employee_payroll_reports
  end

  crumb :alphena_paygrades_edit_paygrade do |grade|
    link I18n.t('update_pay_grade'), {:controller=>"alphena_paygrades", :action=>"edit_paygrade", :id=>grade.id}
    parent :alphena_paygrades_add_paygrade
  end

  crumb :payroll_list_payroll_categories do
    link I18n.t('assign_payroll_categories'), {:controller=>"payroll", :action=>"list_payroll_categories"}
    parent :reports_employee_payroll_reports
  end

  crumb :alphena_paygrades_add_paygrade do
    link I18n.t('add_pay_grade'), {:controller=>"alphena_paygrades", :action=>"add_paygrade"}
    parent :reports_employee_payroll_reports
  end

  crumb :reports_cummulative_report do
    link I18n.t('employee_cumulative_details'), {:controller=>"report", :action=>"cummulative_report"}
    parent :reports_employee_payroll_reports
  end

  crumb :reports_monthly_wise_payslip do
    link I18n.t('salary_structure_report'), {:controller=>"report", :action=>"monthly_wise_payslip"}
    parent :reports_employee_payroll_reports
  end

  crumb :employee_bank_details do
    link I18n.t('bank_report_of_employee'), {:controller=>"employee", :action=>"bank_details"}
    parent :reports_employee_payroll_reports
  end

  crumb :employee_department_wise_monthly_payslip do
    link I18n.t('department_wise_monthly_report'), {:controller=>"employee", :action=>"department_wise_monthly_payslip"}
    parent :reports_employee_payroll_reports
  end
   

########################################
  #costcode module
  ########################################

  crumb :costcodes_new do
     link I18n.t('new_costcode'), {:controller=>"costcodes",:action=>"new"}
     parent :reports_employee_payroll_reports
  end

  crumb :edit_costcode do |costcode|
     link I18n.t('edit_costcode'), {:controller=>"costcodes",:action=>"edit", :id=>costcode.id}
     parent :costcodes_new
  end

  ########################################
  #alphena-audit module
  ########################################

  crumb :alphena_audits_audit_link do
    link I18n.t('data_and_audit_report'), {:controller=>"alphena_audits", :action=>"audit_link"}
  end

  crumb :alphena_audits_alphena_user_audit do
    link I18n.t('view_data_activities'), {:controller=>"alphena_audits", :action=>"alphena_audit_log"}
    parent :alphena_audits_audit_link
  end

  crumb :alphena_audits_alphena_audit_log do
    link I18n.t('view_user_activities'), {:controller=>"alphena_audits", :action=>"alphena_user_audit"}
    parent :alphena_audits_audit_link
  end

  crumb :alphena_audits_alphena_activity do
    link I18n.t('audit_activities'), {:controller=>"alphena_audits", :action=>"alphena_activity"}
    parent :alphena_audits_audit_link
  end

  crumb :alphena_finance_on_revert do
    link I18n.t('finance_on_revert'), {:controller=>"alphena_finance", :action=>"on_revert"}
    parent :finance_index
  end

  crumb :alphena_finance_on_monthly_revert do
    link I18n.t('finance_monthly_on_revert'), {:controller=>"alphena_finance", :action=>"on_monthly_revert"}
    parent :finance_index
  end

  crumb :report_employee_payroll_details do
    link I18n.t('finance_monthly_on_revert'), {:controller=>"report", :action=>"employee_payroll_details"}
    parent :finance_index
  end

  crumb :gifis_mandate_schedule do
    link I18n.t('gifis_mandate_schedule'), {:controller=>"employee", :action=>"gifmis_mandate_report"}
    parent :reports_employee_payroll_reports
  end

  crumb :alphenaemployee_department_wise_monthly_payslip do
    link I18n.t('employee_department_wise_monthly_payslip'), {:controller=>"employee", :action=>"alphena_employee_wise_payslip"}
    parent :alphena_audits_audit_link
  end


 end