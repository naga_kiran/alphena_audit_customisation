menu_link_present = MenuLink rescue false
unless menu_link_present == false
  data_and_reports_category = MenuLinkCategory.find_by_name("data_and_reports")

  MenuLink.create(:name=>'alphena_report_text',:target_controller=>'report',:target_action=>'employee_payroll_reports',:higher_link_id=>nil,:icon_class=>'google-docs-icon',:link_type=>'general',:user_type=>nil,:menu_link_category_id=>data_and_reports_category.id) unless MenuLink.exists?(:name=>'alphena_report_text')
  higher_link=MenuLink.find_by_name_and_higher_link_id('alphena_report_text',nil)

  MenuLink.create(:name=>'alphena_payroll',:target_controller=>'payroll',:target_action=>'list_payroll_categories',:higher_link_id=>higher_link.id,:icon_class=>nil,:link_type=>'general',:user_type=>nil,:menu_link_category_id=>data_and_reports_category.id) unless MenuLink.exists?(:name=>'alphena_payroll')
  MenuLink.create(:name=>'alphena_report',:target_controller=>'report',:target_action=>'employee_payroll_category',:higher_link_id=>higher_link.id,:icon_class=>nil,:link_type=>'general',:user_type=>nil,:menu_link_category_id=>data_and_reports_category.id) unless MenuLink.exists?(:name=>'alphena_report')
  MenuLink.create(:name=>'alphena_paygrades',:target_controller=>'alphena_paygrades',:target_action=>'add_paygrade',:higher_link_id=>higher_link.id,:icon_class=>nil,:link_type=>'general',:user_type=>nil,:menu_link_category_id=>data_and_reports_category.id) unless MenuLink.exists?(:name=>'alphena_paygrades')
  MenuLink.create(:name=>'cummulative_report',:target_controller=>'report',:target_action=>'cummulative_report',:higher_link_id=>higher_link.id,:icon_class=>nil,:link_type=>'general',:user_type=>nil,:menu_link_category_id=>data_and_reports_category.id) unless MenuLink.exists?(:name=>'cummulative_report')

 end

 unless menu_link_present == false
 	 MenuLink.create(:name=>'audit',:target_controller=>'alphena_audits',:target_action=>'audit_link',:higher_link_id=>nil,:icon_class=>'link-icon examination-icon',:link_type=>'general',:user_type=>nil,:menu_link_category_id=>data_and_reports_category.id) unless MenuLink.exists?(:name=>'audit')
  higher_link=MenuLink.find_by_name_and_higher_link_id('audit',nil)

   MenuLink.create(:name=>'alphena_audit_log',:target_controller=>'alphena_audits',:target_action=>'alphena_audit_log',:higher_link_id=>higher_link.id,:icon_class=>nil,:link_type=>'general',:user_type=>nil,:menu_link_category_id=>data_and_reports_category.id) unless MenuLink.exists?(:name=>'alphena_audit_log')
  MenuLink.create(:name=>'alphena_user_audit',:target_controller=>'alphena_audits',:target_action=>'alphena_user_audit',:higher_link_id=>higher_link.id,:icon_class=>nil,:link_type=>'general',:user_type=>nil,:menu_link_category_id=>data_and_reports_category.id) unless MenuLink.exists?(:name=>'alphena_user_audit')

 end
 
PrivilegeTag.find_or_create_by_name_tag(:name_tag => "alphena_audit",:priority => 6)
Privilege.find_or_create_by_name(:name => "AuditManager",:description=>"audit_manager_privilege")
Privilege.reset_column_information
if Privilege.column_names.include?("privilege_tag_id")
  Privilege.find_by_name('AuditManager').update_attributes(:privilege_tag_id=>PrivilegeTag.find_by_name_tag('alphena_audit').id, :priority=>12 )
end
