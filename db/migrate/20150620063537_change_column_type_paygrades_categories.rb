class ChangeColumnTypePaygradesCategories < ActiveRecord::Migration
  def self.up
    change_column :alphena_paygrade_payroll_categories, :amount, :decimal,:precision => 10, :scale => 2
  end

  def self.down
  end
end
