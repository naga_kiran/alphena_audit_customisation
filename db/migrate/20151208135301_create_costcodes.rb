class CreateCostcodes < ActiveRecord::Migration
  def self.up
    create_table :costcodes do |t|
      t.integer :payroll_category_id
      t.string :cost_code_name
      t.timestamps
    end
  end

  def self.down
    drop_table :costcodes
  end
end
