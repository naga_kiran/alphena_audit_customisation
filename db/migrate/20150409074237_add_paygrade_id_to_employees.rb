class AddPaygradeIdToEmployees < ActiveRecord::Migration
  def self.up
    add_column :employees, :paygrade_id, :integer
  end

  def self.down
    remove_column :employees, :paygrade_id
  end
end
