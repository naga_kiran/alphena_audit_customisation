class CreateAlphenaPaygrades < ActiveRecord::Migration
  def self.up
    create_table :alphena_paygrades do |t|

      t.string :name
      t.boolean :status
      t.timestamps
    end
  end

  def self.down
    drop_table :alphena_paygrades
  end
end
