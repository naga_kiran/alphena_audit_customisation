class CreateAlphenaPaygradePayrollCategories < ActiveRecord::Migration
  def self.up
    create_table :alphena_paygrade_payroll_categories do |t|
      t.integer :paygrade_id
      t.integer :payroll_category_id
      t.integer :amount
      t.timestamps
    end
  end

  def self.down
    drop_table :alphena_paygrade_payroll_categories
  end
end
