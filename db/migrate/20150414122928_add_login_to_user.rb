class AddLoginToUser < ActiveRecord::Migration
  def self.up
  	add_column :users, :is_logged_in, :boolean, :default => 0
  end

  def self.down
  	remove_column :users, :is_logged_in, :boolean
  end
end
