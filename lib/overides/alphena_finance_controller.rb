
module AlphenaAuditCustomisation
  module AlphenaFinanceController

    def self.included (base)
      base.instance_eval do
        alias_method_chain :index, :tmpl
      end
    end

    def index_with_tmpl
     @hr = Configuration.find_by_config_value("HR")
     render :template => "alphena_finance/index_with_tmpl"
   end

   def on_revert
    flash[:clear]
    begin
      if @current_user.admin?
     @departments = EmployeeDepartment.active_and_ordered
     @salary_dates = MonthlyPayslip.find(:all,:select => "distinct salary_date")
     if request.post?
      post_data = params[:payslip]
      unless post_data.blank?
        if post_data[:salary_date].present? and post_data[:department_id].present?
          @payslips = MonthlyPayslip.find_and_filter_by_department(post_data[:salary_date],post_data[:department_id])
        else
         flash[:notice] = "#{t('select_salary_date')}"
         redirect_to :action=>"on_revert"
       end
     end
   end
   render :template => "alphena_finance/on_revert"
 else
  flash[:notice] = "Permission Denied"
  redirect_to :controller => :finance, :action => :index
  end
 rescue Exception => e
  Rails.logger.info "Exception in alphena_finance_controller, on_revert action"
  Rails.logger.info e
  flash[:notice] = "Sorry, something went wrong. Please inform administration"
  redirect_to :controller => :finance, :action => :index

end
end

def revert_employee_payslip
  flash[:clear]
  begin
     if @current_user.admin?
    @departments = EmployeeDepartment.active_and_ordered
    @salary_dates = MonthlyPayslip.find(:all,:select => "distinct salary_date")
    @employee = Employee.find(params[:id])
    @finance_trans = MonthlyPayslip.find(:all ,:conditions=>["employee_id = #{params[:id]} AND is_approved "])
    @ft = []
    @finance_trans.each do |finance|
      unless finance.finance_transaction_id.nil?
        @finance_transaction = FinanceTransaction.find(finance.finance_transaction_id)
        unless @finance_transaction.nil?
          @ft << @finance_transaction
        end
      end
    end
    @ft.each do |d|
      d.destroy
    end
    @payslip_record = MonthlyPayslip.find_all_by_employee_id(@employee.id)
    @payslip_record.each do |pr|
          pr.destroy #unless pr.is_approved
        end
        @individual_payslip_record = IndividualPayslipCategory.find_all_by_employee_id(@employee.id)
        unless @individual_payslip_record.nil?
         @individual_payslip_record.each do |ipr|
           ipr.destroy
         end
       end    
       redirect_to :action => "on_revert"
       else
    flash[:notice] = "Permission Denied"
    redirect_to :controller => :finance, :action => :index
      end
     rescue Exception => e
      Rails.logger.info "Exception in alphena_finance_controller, revert_employee_payslip action"
      Rails.logger.info e
      flash[:notice] = "Sorry, something went wrong. Please inform administration"
      redirect_to :controller => :finance, :action => :on_revert
    end
  end

  def on_monthly_revert
    flash[:clear]
    begin
       if @current_user.admin?
      @salary_dates = MonthlyPayslip.all(:select => "distinct salary_date")      
      render :template => "alphena_finance/on_monthly_revert"
      else
    flash[:notice] = "Permission Denied"
    redirect_to :controller => :finance, :action => :index
      end
    rescue Exception => e
      Rails.logger.info "Exception in alphena_finance_controller, on_monthly_revert action"
      Rails.logger.info e
      flash[:notice] = "Sorry, something went wrong. Please inform administration"
      redirect_to :controller => :finance, :action => :index
    end
  end



  def revert_monthly_payslip
    flash[:clear]
    begin
         if @current_user.admin?
      @salary_dates = MonthlyPayslip.all(:select => "distinct salary_date") 
      @finance_trans = MonthlyPayslip.find_all_by_salary_date(params[:payslip][:salary_date])
      @ft = []
      @finance_trans.each do |finance|
       unless finance.finance_transaction_id.nil?
        @finance_transaction = FinanceTransaction.find(finance.finance_transaction_id)
        unless @finance_transaction.nil?
         @ft << @finance_transaction
       end
     end
   end
   @ft.each do |d|
    d.destroy
    puts "#{@ft.inspect}"
  end
  @individual_payslip_record = IndividualPayslipCategory.find_all_by_salary_date(params[:payslip][:salary_date])
  @individual_payslip_record.each do |ipr|
    ipr.destroy
  end
  @monthly_payslips = MonthlyPayslip.find_all_by_salary_date(params[:payslip][:salary_date])
  @monthly_payslips.each do |m|
   m.destroy
 end
 redirect_to :action => "on_monthly_revert"
  else
    flash[:notice] = "Permission Denied"
    redirect_to :controller => :finance, :action => :index
  end
rescue Exception => e
  Rails.logger.info "Exception in alphena_finance_controller, revert_monthly_payslip action"
  Rails.logger.info e
  flash[:notice] = "Sorry, something went wrong. Please inform administration"
  redirect_to :controller => :finance, :action => :on_monthly_revert
end
end



end
end
