module AlphenaAuditCustomisation
  module AlphenaPayrollController


    def self.included (base)
      base.instance_eval do
        alias_method_chain :update_dependent_fields, :tmpl
        alias_method_chain :update_dependent_payslip_fields, :tmpl
        alias_method_chain :manage_payroll, :tmpl
        alias_method_chain :edit_payroll_details, :tmpl
      end
    end


    def add_payroll_value
      @category = PayrollCategory.find(params[:id])
      @category_id = @category.id
      #      @deduction_category = PayrollCategory.find_by_id_and_is_deduction(params[:id], true)
      @categories = PayrollCategory.active.find_all_by_is_deduction(false, :order=> "name ASC")
      @deductionable_categories = PayrollCategory.active.find_all_by_is_deduction(true, :order=> "name ASC")
      respond_to do |format|
        format.js { render :template => 'alphena_payroll/add_payroll_value.rjs' }
      end
    end


    def update_dependent_fields_with_tmpl
      cat_id = params[:cat_id]
      amount = params[:amount]
      @payroll = PayrollCategory.find(cat_id)
      @payroll_association = AlphenaPaygradePayrollCategory.find_by_payroll_category_id(@payroll.id)
      @paygrade_associations = AlphenaPaygradePayrollCategory.find_all_by_paygrade_id(@payroll_association.paygrade_id)
      @dependent_categories = []
      @paygrade_associations.each do |paygrade_assoc|
        @dependent_categories << PayrollCategory.active.find_by_id(paygrade_assoc.payroll_category_id,:conditions=>"status = true")
      end
      render :update do |page|
        @dependent_categories.each do |c|
          unless c.percentage.nil?
            percentage_value = c.percentage
            calculated_amount = FedenaPrecision.set_and_modify_precision(amount.to_f*percentage_value/100)
            page["manage_payroll_#{c.id}_amount"].value = calculated_amount
            page << remote_function(:url  => {:action => "update_dependent_fields"}, :with => "'amount='+ #{calculated_amount} + '&cat_id=' + #{c.id}")
          end
        end
      end
    end

    def update_dependent_payslip_fields_with_tmpl
      cat_id = params[:cat_id]
      amount = params[:amount]
      @payroll = PayrollCategory.find_by_id(cat_id)
      @payroll_association = AlphenaPaygradePayrollCategory.find_by_payroll_category_id(@payroll.id)
      @paygrade_associations = AlphenaPaygradePayrollCategory.find_all_by_paygrade_id(@payroll_association.paygrade_id)
      @dependent_categories = []
      @paygrade_associations.each do |paygrade_assoc|
        @dependent_categories << PayrollCategory.find_by_id(paygrade_assoc.payroll_category_id,:conditions=>"status = true")
      end
      render :update do |page|
        @dependent_categories.each do |c|
          unless c.percentage.nil?
            percentage_value = c.percentage
            calculated_amount =FedenaPrecision.set_and_modify_precision(amount.to_f*percentage_value/100)
            page["manage_payroll_monthly_payslips_attributes_#{c.id}_amount"].value = calculated_amount
            page << remote_function(:url  => {:action => "update_dependent_fields"}, :with => "'amount='+ #{calculated_amount} + '&cat_id=' + #{c.id}")
          end
        end
      end
    end

    def manage_payroll_with_tmpl

      @employee = Employee.find(params[:id])
      @paygrade_associations = AlphenaPaygradePayrollCategory.find_all_by_paygrade_id(@employee.paygrade_id)
      @dependent_categories = []
      @independent_categories = []
      # @independent_categories = PayrollCategory.active.find_all_by_payroll_category_id_and_status(nil, true)
      # @dependent_categories = PayrollCategory.active.find_all_by_status(true, :conditions=>"payroll_category_id != \'\'")
      @paygrade_associations.each do |paygrade_assoc|
        @independent = PayrollCategory.active.find_by_id_and_payroll_category_id_and_status(paygrade_assoc.payroll_category_id,nil, true)
        unless @independent.blank?
          @independent_categories << @independent
        end
        @dependent = PayrollCategory.active.find_by_id_and_status(paygrade_assoc.payroll_category_id, true, :conditions=>"payroll_category_id != \'\'")
        unless @dependent.blank?
          @dependent_categories << @dependent
        end
      end
      payroll_created = EmployeeSalaryStructure.find_all_by_employee_id(@employee.id)
      unless @independent_categories.empty? and @dependent_categories.empty?
        if payroll_created.empty?
          if request.post?

            params[:manage_payroll].each_pair do |k, v|
              current_amount = v['amount'].to_f
              EmployeeSalaryStructure.create(:employee_id => params[:id], :payroll_category_id => k, :amount => FedenaPrecision.set_and_modify_precision(current_amount))
            end
            flash[:notice] = "#{t('data_saved_for')} #{@employee.first_name}.  #{t('new_admission_link')} <a href='/employee/admission1'>Click Here</a>"
            redirect_to :controller => "employee", :action => "profile", :id=> @employee.id and return
          end
        else
          flash[:notice] = "#{t('data_saved_for')} #{@employee.first_name}.  #{t('new_admission_link')} <a href='/employee/admission1'>Click Here</a>"
          redirect_to :controller=>"employee", :action=>"profile", :id=>@employee.id and return
        end
      else
        flash[:notice] = "#{t('data_saved_for')} #{@employee.first_name}.  #{t('new_admission_link')} <a href='/employee/admission1'>Click Here</a>"
        redirect_to :controller=>"employee", :action=>"profile", :id=>@employee.id and return
      end
      render :template => 'alphena_payroll/manage_payroll_with_tmpl'
    end

    def edit_payroll_details_with_tmpl
      @employee = Employee.find(params[:id])
      @paygrade_associations = AlphenaPaygradePayrollCategory.find_all_by_paygrade_id(@employee.paygrade_id)
      @dependent_categories = []
      @independent_categories = []
      @paygrade_associations.each do |paygrade_assoc|
        @independent_categories << PayrollCategory.find_by_id_and_status(paygrade_assoc.payroll_category_id, true)
        @dependent_categories << PayrollCategory.find_by_id_and_status(paygrade_assoc.payroll_category_id, true, :conditions=>"payroll_category_id != \'\'")
      end
      if request.post?
        params[:manage_payroll].each_pair do |k, v|
          current_amount = v['amount'].to_f
          row_id = EmployeeSalaryStructure.find_by_employee_id_and_payroll_category_id(@employee, k)
          unless row_id.nil?
            EmployeeSalaryStructure.update(row_id, :employee_id => params[:id], :payroll_category_id => k,
              :amount => FedenaPrecision.set_and_modify_precision(current_amount).to_f)
          else
            EmployeeSalaryStructure.create(:employee_id => params[:id], :payroll_category_id => k, :amount => '%.2f' % current_amount)
          end

        end
        flash[:notice] = "#{t('data_saved_for')} #{@employee.first_name}"
        redirect_to :controller => "employee", :action => "profile", :id=> @employee.id and return
      end
      render :template => 'alphena_payroll/edit_payroll_details_with_tmpl'
    end

    def assign_payroll_categories
      flash[:clear]
      begin
        @paygrade = AlphenaPaygrade.find(params[:paygrade_id])
        @categories = PayrollCategory.active.find_all_by_is_deduction_and_status_and_is_deleted(false,true,false, :order=> "name ASC")
        @deductionable_categories = PayrollCategory.active.find_all_by_is_deduction_and_status_and_is_deleted(true,true,false, :order=> "name ASC")
        render(:update) do |page|
          page.replace_html 'assign_payroll_categories',:partial=>'alphena_payroll/assign_payroll_categories'
        end
      rescue Exception => e
        Rails.logger.info "Exception in alphena_payroll_controller, assign_payroll_categories action"
        Rails.logger.info e
        flash[:notice] = "Sorry, something went wrong. Please inform administration"
        redirect_to :controller => :payroll, :action => :list_payroll_categories
      end
    end

    def list_payroll_categories
      flash[:clear]
      begin
        @paygrades = AlphenaPaygrade.all
        render :template => 'alphena_payroll/list_payroll_categories'
      rescue Exception => e
        Rails.logger.info "Exception in alphena_payroll_controller, list_payroll_categories action"
        Rails.logger.info e
        flash[:notice] = "Sorry, something went wrong. Please inform administration"
        redirect_to :controller => :reports, :action => :employee_payroll_reports
      end
    end

    def save_payroll_categories
      flash[:clear]
      begin
        if params[:alphena_paygrade].present?
         @error= false
        params[:alphena_paygrade].each_pair do |payroll_category_id, details|
          @paygrade_category = AlphenaPaygradePayrollCategory.find(:first, :conditions => { :paygrade_id => params[:paygrade_id][:id],:payroll_category_id => payroll_category_id  } )
          if @paygrade_category.nil?
            unless details[:payroll_category_id].blank?
              AlphenaPaygradePayrollCategory.create do |pay_cat|
                pay_cat.paygrade_id = params[:paygrade_id][:id]
                pay_cat.payroll_category_id = details[:payroll_category_id]
                pay_cat.amount = details[:amount]
              end
              flash[:notice] = "#{t('flash11')}"
            end
          else
            unless details[:payroll_category_id].blank?
              if @paygrade_category.update_attributes(details)
              else
                flash[:notice] = "#{t('flash13')}"
                @error = nil
              end
            else
              if @paygrade_category.delete
              else
                flash[:notice] = "#{t('flash14')}"
                @error = nil
              end
            end
          end
        end
        else
         flash[:notice] = "#{t('flash15')}"
        end
        flash[:notice] = "#{t('flash12')}" if @error == true
        
        redirect_to :action => :list_payroll_categories
      rescue Exception => e
        Rails.logger.info "Exception in alphena_payroll_controller, save_payroll_categories action"
        Rails.logger.info e
        flash[:notice] = "Sorry, something went wrong. Please inform administration"
        redirect_to :controller => :payroll, :action => :list_payroll_categories
      end
    end


    def update_alphena_payslip_fields
      flash[:clear]
      begin
       cat_id = params[:cat_id]
       amount = params[:amount]
       @dependent_categories = PayrollCategory.active.find_all_by_payroll_category_id(cat_id,:conditions=>"status = true")
       render :update do |page|
        @dependent_categories.each do |c|
          unless c.percentage.nil?
            percentage_value = c.percentage          
            calculated_amount = FedenaPrecision.set_and_modify_precision(amount.to_f*percentage_value/100)          
            page["alphena_paygrade_#{c.id}_amount"].value = calculated_amount
            page << remote_function(:url  => {:action => "update_alphena_payslip_fields"}, :with => "'amount='+ #{calculated_amount} + '&cat_id=' + #{c.id}")
          end
        end
      end
    rescue Exception => e
      Rails.logger.info "Exception in alphena_payroll_controller, update_alphena_payslip_fields action"
      Rails.logger.info e
      flash[:notice] = "Sorry, something went wrong. Please inform administration"
      redirect_to :controller => :payroll, :action => :list_payroll_categories
    end
  end

  def update_alphena_dependent_payslips_fields
    flash[:clear]
    begin
      cat_id = params[:cat_id]
      amount = params[:amount]
      @dependent_categories = PayrollCategory.active.find_all_by_payroll_category_id(cat_id,:conditions=>"status = true")
      render :update do |page|
        @dependent_categories.each do |c|
          unless c.percentage.nil?
            percentage_value = c.percentage
            calculated_amount =FedenaPrecision.set_and_modify_precision(amount.to_f*percentage_value/100)
            page["alphena_paygrade_#{c.id}_amount"].value = calculated_amount
            page << remote_function(:url  => {:action => "update_alphena_payslip_fields"}, :with => "'amount='+ #{calculated_amount} + '&cat_id=' + #{c.id}")
          end
        end
      end
    rescue Exception => e
      Rails.logger.info "Exception in alphena_payroll_controller, update_alphena_dependent_payslips_fields action"
      Rails.logger.info e
      flash[:notice] = "Sorry, something went wrong. Please inform administration"
      redirect_to :controller => :payroll, :action => :list_payroll_categories
    end
  end

end
end
