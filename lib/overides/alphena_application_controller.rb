module AlphenaAuditCustomisation
  module AlphenaApplicationController


 def self.included (base)
      base.instance_eval do
         alias_method_chain :check_if_loggedin, :tmpl
      end
    end




   def check_if_loggedin_with_tmpl
     if session[:user_id]
      redirect_to :controller => 'user', :action => 'dashboard'
    end
   end

   end
end
