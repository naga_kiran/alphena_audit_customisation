# To change this template, choose Tools | Templates
# and open the template in the editor.

module AlphenaAuditCustomisation
  module AlphenaUserController


    def self.included (base)
      base.instance_eval do
        alias_method_chain :login, :tmpl
        alias_method_chain :logout, :tmpl
        alias_method_chain :successful_user_login, :tmpl
      end
    end
    
  def login_with_tmpl
    @institute = Configuration.find_by_config_key("LogoName")
    available_login_authes = FedenaPlugin::AVAILABLE_MODULES.select{|m| m[:name].camelize.constantize.respond_to?("login_hook")}
    selected_login_hook = available_login_authes.first if available_login_authes.count>=1
    if selected_login_hook
      authenticated_user = selected_login_hook[:name].camelize.constantize.send("login_hook",self)
    else
      if request.post? and params[:user]
        @user = User.new(params[:user])
        user = User.active.first(:conditions => ["username LIKE BINARY(?)",@user.username])
        if user.present? and User.authenticate?(@user.username, @user.password)
          authenticated_user = user
        end
      end
    end

    if authenticated_user.present? 
      flash.clear
     
      @user_log = User.find_by_username(@user.username)
       if @user_log.is_logged_in == false
       @user_log.update_attributes(:is_logged_in => true)
       successful_user_login(authenticated_user) and return
     else
      flash[:notice] = " #{user.first_name} #{t('already_logged_in')}"
       render :template => 'alphena_user/login_with_tmpl'
       @user_log.update_attributes(:is_logged_in => false)  
   end
    elsif authenticated_user.blank? and request.post?
      flash[:notice] = "#{t('login_error_message')}"
    end
   end

  def successful_user_login_with_tmpl(user)
    cookies.delete("_fedena_session")
    session[:user_id] = user.id
    #    flash[:notice] = "#{t('welcome')}, #{user.first_name} #{user.last_name}!"
    redirect_to ((session[:back_url] unless (session[:back_url]) =~ /user\/logout$/) || {:controller => 'user', :action => 'dashboard'})
    
  end
    
  def logout_with_tmpl
    current_user.update_attributes(:is_logged_in => false)
    Rails.cache.delete("user_main_menu#{session[:user_id]}")
    Rails.cache.delete("user_autocomplete_menu#{session[:user_id]}")
    current_user.delete_user_menu_caches
    session[:user_id] = nil if session[:user_id]
    session[:language] = nil
    flash[:notice] = "#{t('logged_out')}"
    available_login_authes = FedenaPlugin::AVAILABLE_MODULES.select{|m| m[:name].camelize.constantize.respond_to?("logout_hook")}
    selected_logout_hook = available_login_authes.first if available_login_authes.count>=1
    if selected_logout_hook
      selected_logout_hook[:name].camelize.constantize.send("logout_hook",self,"/")
    else
      redirect_to :controller => 'user', :action => 'login' and return
    end
  end

   
  end
end