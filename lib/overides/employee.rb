module AlphenaAuditCustomisation
  module EmployeeAux

    def self.included (base)
      base.instance_eval do
        alias_method_chain :archive_employee, :tmpl
      end
    end

    def archive_employee_with_tmpl(status)
      self.update_attributes(:status_description => status)
      employee_attributes = self.attributes
      employee_attributes.delete "id"
      employee_attributes.delete "photo_file_size"
      employee_attributes.delete "photo_file_name"
      employee_attributes.delete "photo_content_type"
      employee_attributes.delete "paygrade_id"
      employee_attributes.delete "created_at"
      employee_attributes["former_id"]= self.id
      archived_employee = ArchivedEmployee.new(employee_attributes)
      archived_employee.photo = self.photo if self.photo.file?
      if archived_employee.save
        #      self.user.delete unless self.user.nil?
        employee_salary_structures = self.employee_salary_structures
        employee_bank_details = self.employee_bank_details
        employee_additional_details = self.employee_additional_details
        employee_salary_structures.each do |g|
          g.archive_employee_salary_structure(archived_employee.id)
        end
        employee_bank_details.each do |g|
          g.archive_employee_bank_detail(archived_employee.id)
        end
        employee_additional_details.each do |g|
          g.archive_employee_additional_detail(archived_employee.id)
        end
        self.user.biometric_information.try(:destroy)
        self.user.soft_delete
        self.destroy
      end
    end
  end
  
  module EmployeeDepartmentPayroll

    def payroll_category_total(category,department,salary_date)
      @monthly_value = 0
       
      employees = department.employees
      unless employees.blank?
        employees.each do |employee|
          payslip = MonthlyPayslip.find_by_employee_id_and_salary_date_and_payroll_category_id(employee.id,salary_date.to_date,category.id)
          @monthly_value +=  payslip.nil? ? 0 : payslip.amount.to_f
        end
      end
      return @monthly_value
    end

    def responsiblity_category_total(department,salary_date)
      individual_earning_value = 0
      individual_deduction_value = 0
      employees = department.employees
      unless employees.blank?
        employees.each do |employee|
          individual_earning_payslip = IndividualPayslipCategory.find_all_by_employee_id_and_salary_date_and_is_deduction(employee.id,salary_date.to_date,false)
        unless individual_earning_payslip.empty?
          individual_earning_payslip.each do |earning_payslip|
          individual_earning_value += earning_payslip.amount.to_f
          end
        else
          individual_earning_value += 0
        end
          individual_deduction_payslip = IndividualPayslipCategory.find_all_by_employee_id_and_salary_date_and_is_deduction(employee.id,salary_date.to_date,true)
         unless individual_deduction_payslip.empty?
          individual_deduction_payslip.each do |deduction_payslip|
           individual_deduction_value += deduction_payslip.amount.to_f
          end
        else
          individual_deduction_value += 0
        end
        end
      end
        return_hash = {:earning_value => individual_earning_value,:deduction_value =>  individual_deduction_value    }
    return_hash
    end

  end

end