module AlphenaAuditCustomisation
  module AlphenaEmployeeController


    def self.included (base)
      base.instance_eval do
        alias_method_chain :admission1, :tmpl
        alias_method_chain :edit1, :tmpl
        alias_method_chain :create_monthly_payslip, :tmpl
        # alias_method_chain :one_click_payslip_generation, :tmpl
        alias_method_chain :profile_payroll_details, :tmpl
      end
    end

    def admission1_with_tmpl
      
      @user = current_user
      @paygrades = AlphenaPaygrade.all
      @user_name = @user.username
      @employee1 = @user.employee_record
      @categories = EmployeeCategory.find(:all,:order => "name asc",:conditions => "status = true")
      @positions = []
      @grades = EmployeeGrade.find(:all,:order => "name asc",:conditions => "status = true")
      @departments = EmployeeDepartment.active_and_ordered
      @nationalities = Country.all
      @employee = Employee.new(params[:employee])
      @selected_value = Configuration.default_country
      @last_admitted_employee = Employee.find(:last,:conditions=>"employee_number != 'admin'")
      archived_last_employee = ArchivedEmployee.last(:conditions => ["former_id > ? ",@last_admitted_employee.id])
      unless archived_last_employee.nil?
        @last_admitted_employee = archived_last_employee
      end
      @config = Configuration.find_by_config_key('EmployeeNumberAutoIncrement')

      if request.post?

        unless params[:employee][:employee_number].to_i ==0
          @employee.employee_number= "E" + params[:employee][:employee_number].to_s
        end
        unless @employee.employee_number.to_s.downcase == 'admin'
          if @employee.save
            flash[:notice] = "#{t('flash15')} #{@employee.first_name} #{t('flash16')}"
            redirect_to :controller =>"employee" ,:action => "admission2", :id => @employee.id and return
          end
        else
          @employee.errors.add(:employee_number, "#{t('should_not_be_admin')}")
        end
        @positions = EmployeePosition.find_all_by_employee_category_id(params[:employee][:employee_category_id])
       
        
      end
      render :template => 'alphena_employee/admission1_with_tmpl'
    end


    def edit1_with_tmpl
      @categories = EmployeeCategory.find(:all,:order => "name asc", :conditions => "status = true")
      @positions = EmployeePosition.find(:all)
      @grades = EmployeeGrade.find(:all,:order => "name asc", :conditions => "status = true")
      @paygrades = AlphenaPaygrade.find(:all)
      @departments = EmployeeDepartment.active_and_ordered
      @employee = Employee.find(params[:id])
      unless @employee.gender.nil?
        @employee.gender=@employee.gender.downcase
      end
      @employee_user = @employee.user
      @employee.biometric_id = BiometricInformation.find_by_user_id(@employee.user_id).try(:biometric_id)
      if request.post?
        #@employee.biometric_id = params[:employee][:biometric_id]
        if  params[:employee][:employee_number].downcase != 'admin' or @employee_user.admin
          if @employee.update_attributes(params[:employee])
            flash[:notice] = "#{t('flash15')}  #{@employee.first_name} #{t('flash17')}"
            redirect_to :controller =>"employee" ,:action => "profile", :id => @employee.id and return
          end
        else
          @employee.errors.add(:employee_number, "#{t('should_not_be_admin')}")
        end
      end
      render :template => 'alphena_employee/edit1_with_tmpl'
    end


    def create_monthly_payslip_with_tmpl
      @employee = Employee.find(params[:id])
      #@independent_categories+@dependent_categories
      @paygrade_associations = AlphenaPaygradePayrollCategory.find_all_by_paygrade_id(@employee.paygrade_id)
      unless @paygrade_associations.nil?
        @payroll_categories = []
        @paygrade_associations.each do |paygrade_assoc|
          @payroll_category = PayrollCategory.find_by_id(paygrade_assoc.payroll_category_id, :conditions=>["(payroll_category_id != \'\' or payroll_category_id is NULL) and status=1"])
          unless @payroll_category.nil?
            if @payroll_category.is_deleted == false
              @payroll_categories << @payroll_category
            end
          end
        end
        category_ids=@payroll_categories.collect(&:id)
        @employee_salary_structure=EmployeeSalaryStructure.all(:conditions=>{:employee_id=> params[:id], :payroll_category_id=>category_ids}).group_by(&:payroll_category_id)
      end
      if request.xhr?
        flash[:notice]=nil
        salary_date = Date.parse(params[:salary_date])
        error=0
        unless salary_date.to_date < @employee.joining_date.to_date
          if params[:manage_payroll].present?
            start_date = salary_date - ((salary_date.day - 1).days)
            end_date = start_date + 1.month
            payslip_exists = MonthlyPayslip.find_all_by_employee_id(@employee.id,:conditions => ["salary_date >= ? and salary_date < ?", start_date, end_date])
            if payslip_exists == []
              ActiveRecord::Base.transaction do
                error=1  unless @employee.update_attributes(:monthly_payslips_attributes=>params[:manage_payroll][:monthly_payslips_attributes])
                if params[:new_category].present?
                  error=1  unless @employee.update_attributes(:individual_payslip_categories_attributes=>params[:new_category][:individual_payslip_categories_attributes])
                end
                if error==1
                  raise ActiveRecord::Rollback
                end
              end
              flash[:notice] = I18n.t('employee.flash27',:date=>I18n.l(params[:salary_date].to_date,:format=>:month_year),:user=>@employee.first_name)
            else
              flash[:notice] = I18n.t('employee.flash28',:date=>I18n.l(params[:salary_date].to_date,:format=>:month_year),:user=>@employee.first_name)
            end
          else
            error=1
            @employee.errors.add_to_base("#{t('flash51')}")
          end
        else
          error=1
          @employee.errors.add_to_base("#{t('flash45')} #{params[:salary_date]}")
        end
        if error==1
          render :update do |page|
            page.replace_html 'errors', :partial => 'errors', :object => @employee
          end
        else
          privilege = Privilege.find_by_name("FinanceControl")
          finance_manager_ids = privilege.user_ids
          subject = t('payslip_generated')
          body = "#{t('payslip_generated_for')}  "+@employee.first_name+" "+@employee.last_name+". #{t('kindly_approve')}"
          Delayed::Job.enqueue(DelayedReminderJob.new( :sender_id  => current_user.id,
              :recipient_ids => finance_manager_ids,
              :subject=>subject,
              :body=>body ))
          render :update do |page|
            page.redirect_to :controller => "employee", :action => "select_department_employee" 
          end
        end
      else
        render :template => 'alphena_employee/create_monthly_payslip_with_tmpl'  and return
      end
    end

    def one_click_payslip_generation_with_tmpl
      @user = current_user
      @employee = Employee.find_by_id(params[:employee_id])
      @paygrade_associations = AlphenaPaygradePayrollCategory.find_all_by_paygrade_id(@employee.paygrade_id)
      @payroll_categories = []
      @paygrade_associations.each do |paygrade_assoc|
        @payroll_categories << PayrollCategory.find_by_id(paygrade_assoc.payroll_category_id, :conditions=>["(payroll_category_id != \'\' or payroll_category_id is NULL) and status=1"])
      end
      finance_manager = find_finance_managers
      finance = Configuration.find_by_config_value("Finance")
      subject = "#{t('payslip_generated')}"
      body = "#{t('message_body')}"
      salary_date = Date.parse(params[:salary_date])
      start_date = salary_date - ((salary_date.day - 1).days)
      end_date = start_date + 1.month
      employees = Employee.find(:all,:conditions=>["joining_date<=?",salary_date])
      unless(finance_manager.nil? and finance.nil?)
        finance_manager_ids = Privilege.find_by_name('FinanceControl').user_ids
        Delayed::Job.enqueue(DelayedReminderJob.new( :sender_id  => @user.id,
            :recipient_ids => finance_manager_ids,
            :subject=>subject,
            :body=>body ))
      end
      employees.each do|e|
        payslip_exists = MonthlyPayslip.find_all_by_employee_id(e.id,
          :conditions => ["salary_date >= ? and salary_date < ?", start_date, end_date])
        if payslip_exists == []
          category_ids=@payroll_categories.collect(&:id)
          salary_structure=EmployeeSalaryStructure.all(:conditions=>{:employee_id=>e.id,:payroll_category_id=>category_ids})
          unless salary_structure == []
            salary_structure.each do |ss|
              MonthlyPayslip.create(:salary_date=>start_date,
                :employee_id=>e.id,
                :payroll_category_id=>ss.payroll_category_id,
                :amount=>ss.amount,:is_approved => false,:approver => nil)
            end
          end
        end
      end
      render :text => "<p>#{t('salary_slip_for_month')}: #{salary_date.strftime("%B")}.<br/><b>#{t('note')}:</b> #{t('employees_salary_generated_manually')}</p>"
    end

    def profile_payroll_details_with_tmpl
      @employee = Employee.find(params[:id])
      @active_payroll_count=PayrollCategory.active.count(:conditions=>{:status=>true})
      @payroll_details = EmployeeSalaryStructure.all(:select=>"employee_salary_structures.id,employee_id,amount,payroll_categories.name,payroll_categories.is_deduction,employee_salary_structures.payroll_category_id",:conditions=>{:employee_id=>params[:id],:payroll_categories=>{:status=>true}},:joins=>[:payroll_category],:order=>"payroll_category_id")
      render :partial => "alphena_employee/payroll_details"
    end


    def bank_details

      @departments = EmployeeDepartment.all
      if request.post?
        post_data = params[:department_id]
        if post_data[:department_id] == "All"
          @departments = EmployeeDepartment.all
        end
        bank_fields = ["Account Number","Sort Code","Bank Name","Bank Branch"]
        bank_fields.each do |field|
          @bank_field = BankField.find_by_name(field)
          if @bank_field.nil?
            @bank_field = BankField.new
            @bank_field.name = field
            @bank_field.status = true
            @bank_field.save
          end
        end
      end
      render :template => "alphena_employee/bank_details"
    end

    def update_bank_details
      @bank_details = {}
      @bank_fields = BankField.find_all_by_name(["Account Number","Sort Code","Bank Name","Bank Branch"])
      if params[:department_id] == "All"
        @employees = Employee.all
      else
        @employees = Employee.find_all_by_employee_department_id(params[:department_id])
      end
      unless @employees.blank?
        @employees.each_with_index do |employee,i|
          bank_values = {}
          bank_values["emp_name"] = employee.full_name
          bank_values["emp_number"] = employee.employee_number
          @monthly = MonthlyPayslip.find_all_by_employee_id(employee)
          unless @monthly.blank?
            @monthly.each do |id|
              if id.is_rejected == false
                @remark = id.remark
              else
                @remark = id.reason
              end
            end
            date = @monthly.last.salary_date
            @monthly_payslip = MonthlyPayslip.find_all_by_employee_id_and_salary_date(employee,date)
            @individual_payslip_category = IndividualPayslipCategory.find_all_by_employee_id_and_salary_date(employee,date)
            @salary = Employee.calculate_salary(@monthly_payslip,@individual_payslip_category)
          else
            @salary = ""
            @remark = ""
          end
          bank_values["salary"] =@salary[:net_amount]
          bank_values["remark"] =@remark
          @bank_fields.each do |bank_field|
            employee_bank_detail =  EmployeeBankDetail.find_by_employee_id_and_bank_field_id(employee.id,bank_field.id)
            unless employee_bank_detail.nil?
              bank_values["#{bank_field.name}"] =  employee_bank_detail.bank_info
            else
              bank_values["#{bank_field.name}"] = "please add #{bank_field.name.downcase}"
            end
          end
          @bank_details["#{i}"] =  bank_values
        end
        @bank_details_values = @bank_details.sort_by {|k,v| v["Bank Name"]}
      end
      render :update do |page|
        page.replace_html 'employee_list', :partial => 'alphena_employee/bank_all'
      end
    end

    def generate_csv_file
      flash[:clear]
      begin
        @bank_details = {}
        @bank_fields = BankField.find_all_by_name(["Account Number","Sort Code","Bank Name","Bank Branch"])
        payroll_csv = FasterCSV.generate({:col_sep => "\t"}) do |csv|
          #header rows
          csv << ["","","","#{t('bank_text')} #{t('report')}"]
          # data rows
          csv << ["#{t('sl_no')}", "#{t('employee_name1')}","#{t('employee_id1')}","#{t('account_number')}","#{t('sort_code')}","#{t('bank_name')}","#{t('bank_branch')}","#{t('net_salary')}","#{t('payment_purpose')}"]
          
          if params[:department_id] == "All"
            @employees = Employee.all
          else
            @employees = Employee.find_all_by_employee_department_id(params[:department_id])
          end
      
          @employees.each_with_index do |employee,i|
         bank_values = {}
          bank_values["emp_name"] = employee.full_name
          bank_values["emp_number"] = employee.employee_number
          @monthly = MonthlyPayslip.find_all_by_employee_id(employee)
          unless @monthly.blank?
            @monthly.each do |id|
              if id.is_rejected == false
                @remark = id.remark
              else
                @remark = id.reason
              end
            end
            date = @monthly.last.salary_date
            @monthly_payslip = MonthlyPayslip.find_all_by_employee_id_and_salary_date(employee,date)
            @individual_payslip_category = IndividualPayslipCategory.find_all_by_employee_id_and_salary_date(employee,date)
            @salary = Employee.calculate_salary(@monthly_payslip,@individual_payslip_category)
          else
            @salary = ""
            @remark = ""
          end
          bank_values["salary"] =@salary[:net_amount]
          bank_values["remark"] =@remark
          @bank_fields.each do |bank_field|
            employee_bank_detail =  EmployeeBankDetail.find_by_employee_id_and_bank_field_id(employee.id,bank_field.id)
            unless employee_bank_detail.nil?
              bank_values["#{bank_field.name}"] =  employee_bank_detail.bank_info
            else
              bank_values["#{bank_field.name}"] = "please add #{bank_field.name.downcase}"
            end
          end
          @bank_details["#{i}"] =  bank_values
          end
          @bank_details_values = @bank_details.sort_by {|k,v| v["Bank Name"]}
              @total = 0
             count = 0
          @bank_details_values.each do |k,v|
            bank_values =[]
             @bank_fields.each do |bank_field|
             bank_values  << v["#{bank_field.name}"]
             end
           csv << ["#{count+=1}","#{ v["emp_name"]}","#{ v["emp_number"]}"] + bank_values + ["#{v["salary"]}","#{ v["remark"]}"]
         @total += v["salary"].to_f
          end
          csv << ["",t('total'),"","","","","",@total]
        end
      end
      send_data(payroll_csv, :type => 'text/csv; charset=utf-8; header=present', :filename => "bankdetails.csv")
    rescue Exception => e
      Rails.logger.info "Exception in alphena_report_controller, generate_csv_file action"
      Rails.logger.info e
      flash[:notice] = "Sorry, something went wrong. Please inform administration"
      redirect_to :controller => :employee, :action => :bank_details
      # end
    end

    def department_wise_monthly_payslip
      @salary_dates = MonthlyPayslip.find(:all,:select => "distinct salary_date")
      if request.post?
        post_data = params[:payslip]
        unless post_data.blank?
          flash[:clear]
          if post_data[:salary_date].present?
            @category_id_payslips = MonthlyPayslip.find_all_by_salary_date(post_data[:salary_date]).group_by{|e| e.employee.employee_position.employee_category_id}
            @category_departments = {}
            @category_id_payslips.each do |k,v|
              department_ids = v.map{|e| e.employee.employee_department_id }.uniq
              @employee_departments = EmployeeDepartment.find_all_by_id(department_ids)
              @category_departments["#{k}"] = @employee_departments
            end
            @payroll_category = PayrollCategory.active
            @grouped_payslip_category = @payroll_category.group_by(&:is_deduction) unless @payroll_category.empty?
          else
            flash[:notice] = "#{t('select_salary_date')}"
          end
        end
      end
      render :template => "alphena_employee/monthly_wise_department_payslip"
    end

    def department_wise_monthly_payslip_csv
      flash[:clear]
      begin
        earnings = []
        deductions = []
        @category_id_payslips =MonthlyPayslip.find_all_by_salary_date(params[:start_date]).group_by{|e| e.employee.employee_position.employee_category_id}
        @category_departments = {}
        @category_id_payslips.each do |k,v|
          department_ids = v.map{|e| e.employee.employee_department_id }.uniq
          @employee_departments = EmployeeDepartment.find_all_by_id(department_ids)
          @category_departments["#{k}"] = @employee_departments
        end
        # @employee_departments = EmployeeDepartment.all
        @payroll_category = PayrollCategory.active
        @grouped_payslip_category = @payroll_category.group_by(&:is_deduction) unless @payroll_category.empty?

        unless @category_departments.blank?
          hash_total_grand = {}
          unless  @grouped_payslip_category.nil?
            if @grouped_payslip_category[false].present?
              @grouped_payslip_category[false].each do |cat|
                earnings  << cat.name
              end
              # earnings  << t('responsibility')
              earnings << t('total_areas_and_allowances')
            end
            if @grouped_payslip_category[true].present?
              @grouped_payslip_category[true].each do |cat|
                deductions << cat.name
              end
              # deductions  << t('individual_deductions')
              deductions << t('total_deductions')
            end
          end   
          payroll_csv = FasterCSV.generate({:col_sep => "\t"}) do |csv|
            csv << ["#{t('sl_no')}","#{t('details')}"] + earnings +["#{t('gross_amount')}"] + deductions + ["#{t('net_amount')}"]
            @category_departments.each do | k,v|
              hash_total = {}
              v.each_with_index do |depart,i|
                total_allowences = 0
                total_deductions = 0
                values = []
                values << i+1
                values << depart.name
                unless  @grouped_payslip_category.nil?
                  if @grouped_payslip_category[false].present?
                    @grouped_payslip_category[false].each do |cat|
                      @earning_cat_value = Employee.payroll_category_total(cat,depart,params[:start_date])
                      if hash_total["#{cat.id }"].nil?
                        hash_total["#{cat.id }"] = @earning_cat_value
                      else
                        hash_total["#{cat.id }"] += @earning_cat_value
                      end
                      @earning_cat_value == 0 ?  values << "" :  values << @earning_cat_value
                      total_allowences += @earning_cat_value
                    end
                    total_allowences == 0 ? values <<  "" :  values <<  total_allowences
                    if hash_total["total_allowences"].nil?
                      hash_total["total_allowences"] = total_allowences.to_f
                    else
                      hash_total["total_allowences"] += total_allowences.to_f
                    end
                  end
                  if @grouped_payslip_category[true].present?
                    @grouped_payslip_category[true].each do |cat|
                      @deduction_cat_value = Employee.payroll_category_total(cat,depart,params[:start_date])
                      if hash_total["#{cat.id }"].nil?
                        hash_total["#{cat.id }"] = @deduction_cat_value
                      else
                        hash_total["#{cat.id }"] += @deduction_cat_value
                      end
                      total_deductions += @deduction_cat_value
                    end
                  end
                  gross =  total_allowences+total_deductions
                  if hash_total["gross"].nil?
                    hash_total["gross"] =  gross
                  else
                    hash_total["gross"] +=  gross
                  end
                  gross == 0 ? values <<  "" : values << gross
                  if @grouped_payslip_category[true].present?
                    @grouped_payslip_category[true].each do |cat|
                      @deduction_cat_value = Employee.payroll_category_total(cat,depart,params[:start_date])
                      @deduction_cat_value == 0 ? values << "" :  values << @deduction_cat_value
                    end
                    total_deductions == 0 ? values << "" : values << total_deductions
                    if hash_total["total_deductions"].nil?
                      hash_total["total_deductions"] = total_deductions.to_f
                    else
                      hash_total["total_deductions"] +=  total_deductions.to_f
                    end
                  end
                  net_salary =   total_allowences-total_deductions
                  if hash_total["net_salary"].nil?
                    hash_total["net_salary"] =  net_salary
                  else
                    hash_total["net_salary"] +=  net_salary
                  end
                  net_salary == 0 ? values << "" : values << net_salary
                end
                csv << values
              end

              #Total
              category_values = []
              category_values << ""
              category_values << EmployeeCategory.find(k).name
              unless  @grouped_payslip_category.nil?
                if @grouped_payslip_category[false].present?
                  @grouped_payslip_category[false].each do |cat|
                    category_values << hash_total["#{cat.id}"]
                    if hash_total_grand["#{cat.id}"].nil?
                      hash_total_grand["#{cat.id}"] = hash_total["#{cat.id}"].to_f
                    else
                      hash_total_grand["#{cat.id}"] +=  hash_total["#{cat.id}"].to_f
                    end
                  end
                  category_values << hash_total["total_allowences"]
                  if hash_total_grand["total_allowences"].nil?
                    hash_total_grand["total_allowences"] = hash_total["total_allowences"].to_f
                  else
                    hash_total_grand["total_allowences"] +=  hash_total["total_allowences"].to_f
                  end
                end
                category_values << hash_total["gross"]
                if hash_total_grand["gross"].nil?
                  hash_total_grand["gross"] = hash_total["gross"].to_f
                else
                  hash_total_grand["gross"] +=  hash_total["gross"].to_f
                end
                if @grouped_payslip_category[true].present?
                  @grouped_payslip_category[true].each do |cat|
                    category_values << hash_total["#{cat.id}"]
                    if hash_total_grand["#{cat.id}"].nil?
                      hash_total_grand["#{cat.id}"] = hash_total["#{cat.id}"].to_f
                    else
                      hash_total_grand["#{cat.id}"] +=  hash_total["#{cat.id}"].to_f
                    end
                  end
                  category_values << hash_total["total_deductions"]
                  if hash_total_grand["total_deductions"].nil?
                    hash_total_grand["total_deductions"] = hash_total["total_deductions"].to_f
                  else
                    hash_total_grand["total_deductions"] +=  hash_total["total_deductions"].to_f
                  end
                end
                category_values << hash_total["net_salary"]
                if hash_total_grand["net_salary"].nil?
                  hash_total_grand["net_salary"] = hash_total["net_salary"].to_f
                else
                  hash_total_grand["net_salary"] +=  hash_total["net_salary"].to_f
                end
              end
              csv << category_values


              #  end



            end
            grand_total_value = []
            grand_total_value << ""
            grand_total_value << "Grand Total"
            unless  @grouped_payslip_category.nil?
              if @grouped_payslip_category[false].present?
                @grouped_payslip_category[false].each do |cat|
                  grand_total_value << hash_total_grand["#{cat.id}"]
                end
                grand_total_value << hash_total_grand["total_allowences"]
              end
              grand_total_value << hash_total_grand["gross"]
              if @grouped_payslip_category[true].present?
                @grouped_payslip_category[true].each do |cat|
                  grand_total_value << hash_total_grand["#{cat.id}"]
                end
                                    
                grand_total_value << hash_total_grand["total_deductions"]
              end
              grand_total_value << hash_total_grand["net_salary"]
                     
            end
            csv << grand_total_value
          end
        end

        send_data(payroll_csv, :type => 'text/csv; charset=utf-8; header=present', :filename => "department_wise_payroll_details.csv")
      rescue Exception => e
        Rails.logger.info "Exception in alphena_employee_controller, department_wise_monthly_payslip_csv action"
        Rails.logger.info e
        flash[:notice] = "Sorry, something went wrong. Please inform administration"
        redirect_to :controller => :report, :action => :employee_payroll_details
      end
    end

    def gifmis_mandate_report
      @departments = EmployeeDepartment.all
      if request.post?
        post_data = params[:department_id]
        if post_data[:department_id] == "All"
          @departments = EmployeeDepartment.all
        end
        bank_fields = ["Account Number"]
        bank_fields.each do |field|
          @bank_field = BankField.find_by_name(field)
          if @bank_field.nil?
            @bank_field = BankField.new
            @bank_field.name = field
            @bank_field.status = true
            @bank_field.save
          end
        end
      end
      render :template => "alphena_employee/gifmis_mandate_report"
    end

    def update_gifmis_mandate_report
      @departments = EmployeeDepartment.active_and_ordered
      @bank_field = BankField.find_by_name(["Account Number"])
      @employees = Employee.find_all_by_employee_department_id(params[:department_id])
      if params[:department_id] == "All"
        @employees = Employee.all
      end

      render :update do |page|
        page.replace_html 'gifis_list', :partial => 'alphena_employee/gifims'
      end
    end

    def gifis_csv_file
      flash[:clear]
      begin
        @bank_fields = BankField.find_by_name(["Account Number"])
        payroll_csv = FasterCSV.generate({:col_sep => "\t"}) do |csv|
          #header rows
          csv << ["","","","FEDERAL UNIVERSITY GUSUV"]
          csv << ["","","","GIFMIS MANDATE SCHEDULE"]
          # data rows
          csv << [ "#{t('account_number')}","#{t('net_salary')}","#{t('currency')}","#{t('coa_accont')}","#{t('fund')}","#{t('program')}","#{t('description')}","#{t('phone')}","#{t('email')}","#{t('reference_no')}"]
          @employees = Employee.find_all_by_employee_department_id(params[:department_id])
          if params[:department_id] == "All"
            @employees = Employee.all
          end
          @total = 0.to_f
          @employees.each do |employee|

            @monthly = MonthlyPayslip.find_all_by_employee_id(employee)
            unless @monthly.blank?
              @monthly.each do |id|
                if id.is_rejected == false
                  unless id.remark.nil? 
                    @remark = id.remark.gsub(',', " ")
                  else
                    @remark = " "
                  end
                else
                  unless id.reason.nil?
                    @remark = id.reason.gsub(',', " ")
                  else
                    @remark = " "
                  end
                end
              end
              # end
              remark = @monthly.last.remark
              date = @monthly.last.salary_date
              @monthly_payslip = MonthlyPayslip.find_all_by_employee_id_and_salary_date(employee,date)
              @individual_payslip_category = IndividualPayslipCategory.find_all_by_employee_id_and_salary_date(employee,date)
              @salary = Employee.calculate_salary(@monthly_payslip,@individual_payslip_category)
            else
              @salary = ""
              remark = ""
            end
            bank_details = []
            net_salary = []
            purpose_of_payment = []
            net_salary << @salary[:net_amount].to_f
            purpose_of_payment << @remark
              employee_bank_detail =  EmployeeBankDetail.find_by_employee_id_and_bank_field_id(employee.id,@bank_fields.id)
              unless employee_bank_detail.nil?
                bank_details <<  employee_bank_detail.bank_info
              else
                bank_details << ""
              end

            @temp = @salary[:net_amount].to_f
            if @temp != 0
              @total = @total + @temp
            else
              @total
            end
            c = "#{@monthly_payslip.first.salary_date.year.to_a unless @monthly_payslip.nil?}#{employee.id}"
            # csv <<  bank_details + net_salary +["NGN","21010101","02101","" ]+ purpose_of_payment +[employee.mobile_phone,employee.email] + [@monthly_payslip.first.salary_date.year.to_a unless @monthly_payslip.nil?, employee.id].join(" ")
            csv <<  bank_details + net_salary +["NGN","21010101","02101","" ]+ purpose_of_payment +[employee.mobile_phone,employee.email] + c.to_a
          end
          csv << [t('total'),@total]
        end
      end
      send_data(payroll_csv, :type => 'text/csv; charset=utf-8; header=present', :filename => "gifis_schedule.csv")
    rescue Exception => e
      Rails.logger.info "Exception in alphena_report_controller, generate_csv_file action"
      Rails.logger.info e
      flash[:notice] = "Sorry, something went wrong. Please inform administration"
      redirect_to :controller => :employee, :action => :gifmis_mandate_report
      # end
    end

    def alphena_employee_wise_payslip
      @salary_dates = MonthlyPayslip.find(:all, :select => "distinct salary_date")
      render :template => "alphena_employee/alphena_employee_wise_payslip"
    end

    def alphena_employee_wise_payslip_pdf

      @payslips = MonthlyPayslip.find_and_filter_by_department(params[:payslip][:salary_date], "All")
      if  @payslips[:monthly_payslips].present? or @payslips[:individual_payslip_category].present?
        @grouped_monthly_payslips = @payslips[:monthly_payslips] unless @payslips[:monthly_payslips].blank?
        render :pdf => "alphena_employee/alphena_employee_wise_payslip_pdf",
          :template => "alphena_employee/alphena_employee_wise_payslip_pdf"
      end
  
    end

  end
end