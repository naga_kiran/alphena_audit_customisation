module AlphenaAuditCustomisation
  module AlphenaReportController


    def self.included (base)
      base.instance_eval do
        alias_method_chain :employee_payroll_details ,:tmpl
        alias_method_chain :employees ,:tmpl
      end
    end

    def employee_payroll_details_with_tmpl
      @departments=EmployeeDepartment.active_and_ordered(:select=>"name,id")
      @sort_order=params[:sort_order]
      if params[:department_id].nil? or params[:department_id].blank?
        if @sort_order.nil?
          @employees= Employee.paginate(:select=>"first_name,middle_name,last_name,employees.id,employee_departments.name as department_name,count(employee_salary_structures.id) as emp_sub_count,employee_number",:joins=>[:employee_salary_structures,:employee_department],:group=>"employees.id",:page=>params[:page],:per_page=>20 ,:order=>'first_name ASC')
          emp_ids=@employees.collect(&:id)
          @payroll=EmployeeSalaryStructure.all(:select=>"employee_id,amount,payroll_categories.name,payroll_categories.is_deduction",:joins=>[:payroll_category],:conditions=>["payroll_categories.status=? and employee_id IN (?)",true,emp_ids],:order=>'name ASC').group_by(&:employee_id)
        else
          @employees= Employee.paginate(:select=>"first_name,middle_name,last_name,employees.id,employee_departments.name as department_name,count(employee_salary_structures.id) as emp_sub_count,employee_number",:joins=>[:employee_salary_structures,:employee_department],:group=>"employees.id",:page=>params[:page],:per_page=>20 ,:order=>@sort_order)
          emp_ids=@employees.collect(&:id)
          @payroll=EmployeeSalaryStructure.all(:select=>"employee_id,amount,payroll_categories.name,payroll_categories.is_deduction",:joins=>[:payroll_category],:conditions=>["payroll_categories.status=? and employee_id IN (?)",true,emp_ids],:order=>'name ASC').group_by(&:employee_id)
        end
      else
        if @sort_order.nil?
          @employees= Employee.paginate(:select=>"first_name,middle_name,last_name,employees.id,employee_departments.name as department_name,count(employee_salary_structures.id) as emp_sub_count,employee_number",:joins=>[:employee_salary_structures,:employee_department],:group=>"employees.id",:conditions=>["employee_departments.id=?",params[:department_id]],:page=>params[:page],:per_page=>20 ,:order=>'first_name ASC')
          emp_ids=@employees.collect(&:id)
          @payroll=EmployeeSalaryStructure.all(:select=>"employee_id,amount,payroll_categories.name,payroll_categories.is_deduction",:joins=>[:payroll_category],:conditions=>["payroll_categories.status=? and employee_id IN (?)",true,emp_ids],:order=>'name ASC').group_by(&:employee_id)
        else
          @employees= Employee.paginate(:select=>"first_name,middle_name,last_name,employees.id,employee_departments.name as department_name,count(employee_salary_structures.id) as emp_sub_count,employee_number",:joins=>[:employee_salary_structures,:employee_department],:group=>"employees.id",:conditions=>["employee_departments.id=?",params[:department_id]],:page=>params[:page],:per_page=>20 ,:order=>@sort_order)
          emp_ids=@employees.collect(&:id)
          @payroll=EmployeeSalaryStructure.all(:select=>"employee_id,amount,payroll_categories.name,payroll_categories.is_deduction",:joins=>[:payroll_category],:conditions=>["payroll_categories.status=? and employee_id IN (?)",true,emp_ids],:order=>'name ASC').group_by(&:employee_id)
        end
      end
      if request.xhr?
        render :update do |page|
          page.replace_html "information", :partial => "alphena_report/payroll_details"
        end
      else
        render :template => "alphena_report/employee_payroll_details_with_tmpl" and return
      end
    end

    def payrolls_details_csv_file
      flash[:clear]
      begin
        payroll_csv = FasterCSV.generate({:col_sep => "\t"}) do |csv|
          #header rows
          csv << ["","","","#{t('employee_text')} #{t('payroll_text')} #{t('details')}"]
          # data rows
          csv << [ t('no_text'), t('name'), t('employee_id'),t('department'),t('payroll_category'), t('amount'),]
          if params[:department_id].nil? or params[:department_id].blank?
            @employees= Employee.all(:select=>"first_name,middle_name,last_name,employees.id,employee_departments.name as department_name,count(employee_salary_structures.id) as emp_sub_count,employee_number",:joins=>[:employee_salary_structures,:employee_department],:group=>"employees.id",:order=>'first_name ASC')
            emp_ids=@employees.collect(&:id)
            @payroll=EmployeeSalaryStructure.all(:select=>"employee_id,amount,payroll_categories.name,payroll_categories.is_deduction",:joins=>[:payroll_category],:conditions=>["payroll_categories.status=? and employee_id IN (?)",true,emp_ids],:order=>'name ASC').group_by(&:employee_id)
          else
            @employees= Employee.all(:select=>"first_name,middle_name,last_name,employees.id,employee_departments.name as department_name,count(employee_salary_structures.id) as emp_sub_count,employee_number",:joins=>[:employee_salary_structures,:employee_department],:group=>"employees.id",:conditions=>["employee_departments.id=?",params[:department_id]])
            emp_ids=@employees.collect(&:id)
            @payroll=EmployeeSalaryStructure.all(:select=>"employee_id,amount,payroll_categories.name,payroll_categories.is_deduction",:joins=>[:payroll_category],:conditions=>["payroll_categories.status=? and employee_id IN (?)",true,emp_ids],:order=>'name ASC').group_by(&:employee_id)
          end
          i = 0
          @employees.each do |e|
            csv << [  i+=1,e.full_name,e.employee_number,e.department_name]
            payroll=@payroll[e.id]
            total=0.to_f
            payroll.each do |p|
              if p.is_deduction=="1"
                csv << ["","","","",p.name,p.amount]
                total -= p.amount.to_f
              else
                csv << ["","","","",p.name,p.amount]
                total += p.amount.to_f
              end
            end
            csv << ["","","","",t('total'),total]
          end
        end
        send_data(payroll_csv, :type => 'text/csv; charset=utf-8; header=present', :filename => "employees_payroll_details.csv")
      rescue Exception => e
        Rails.logger.info "Exception in alphena_report_controller, payrolls_details_csv_file action"
        Rails.logger.info e
        flash[:notice] = "Sorry, something went wrong. Please inform administration"
        redirect_to :controller => :report, :action => :employee_payroll_details
      end
    end

    def employee_payroll_reports
      render :template => "alphena_report/employee_payroll_reports"
    end

    def employee_payroll_category
      flash[:clear]
      begin
        @category = PayrollCategory.all
        if request.post?
          @additional_field_name = AdditionalField.find_all_by_name(["Rank","Nature of Appointment","Salary Structure","GL Level"])
          flash.clear
          post_data = params[:payroll]
          unless params[:start_date] > params[:end_date]
            unless post_data.blank?
              unless post_data[:payroll_cat] == "All" or post_data[:payroll_cat] == "gross" or post_data[:payroll_cat] == "net_salary" or post_data[:payroll_cat] == "deduction" or post_data[:payroll_cat] == "individual_earnings" or post_data[:payroll_cat] == "individual_deductions"
                @payrolls =  MonthlyPayslip.all(:conditions =>["payroll_category_id = :pay_cat AND salary_date >= :start_date AND salary_date <= :end_date", { :start_date => params[:start_date], :end_date => params[:end_date],:pay_cat => post_data[:payroll_cat].to_i }])
              end
              if post_data[:payroll_cat] == "All" or post_data[:payroll_cat] == "gross"
                @categories =  MonthlyPayslip.all(:conditions =>["salary_date >= :start_date AND salary_date <= :end_date", { :start_date => params[:start_date], :end_date => params[:end_date] }])
              end
              if post_data[:payroll_cat] == "net_salary"
                @gross_salary =  MonthlyPayslip.all(:conditions =>["salary_date >= :start_date AND salary_date <= :end_date", { :start_date => params[:start_date], :end_date => params[:end_date] }])
                @categories = []
                @gross_salary.each do |gross|
                  @earning = PayrollCategory.find_by_id_and_status_and_is_deleted(gross.payroll_category_id,true,false).is_deduction
                  if @earning == false
                    @categories << gross
                  end
                end
              end
              if post_data[:payroll_cat] == "deduction"
                @gross_salary =  MonthlyPayslip.all(:conditions =>["salary_date >= :start_date AND salary_date <= :end_date", { :start_date => params[:start_date], :end_date => params[:end_date] }])
                @categories = []
                @gross_salary.each do |deduction|
                  @earning = PayrollCategory.find_by_id_and_status_and_is_deleted(deduction.payroll_category_id,true,false).is_deduction
                  if @earning == true
                    @categories << deduction
                  end
                end
              end
              if post_data[:payroll_cat] == "individual_earnings"
                @responsibility = IndividualPayslipCategory.all(:conditions =>["salary_date >= ? and salary_date <= ? and is_deduction = ? ",params[:start_date],params[:end_date],false] )
              end
              if post_data[:payroll_cat] == "individual_deductions"
                @individual_deductions = IndividualPayslipCategory.all(:conditions =>["salary_date >= ? and salary_date <= ? and is_deduction = ? ",params[:start_date],params[:end_date],true] )
                @hash_value = @individual_deductions.group_by{ |s| [s.employee_id, s.salary_date]}
              end
            else
              flash[:notice] = "#{t('no_display')}"
            end
          else
            flash[:notice] = "#{t('date_range')}"
          end
        end
        render :template => "alphena_report/employee_payroll_category"
      rescue Exception => e
        Rails.logger.info "Exception in alphena_report_controller, employee_payroll_category action"
        Rails.logger.info e
        flash[:notice] = "Sorry, something went wrong. Please inform administration"
        redirect_to :controller => :report, :action => :employee_payroll_reports
      end
    end

    def employee_payroll_pdf
      flash[:clear]
      begin
        unless params[:payroll_cat].blank?
          unless params[:payroll_cat] == "All" or params[:payroll_cat] == "gross" or params[:payroll_cat] == "net_salary" or params[:payroll_cat] == "deduction" or params[:payroll_cat] == "individual_earnings"
            @payrolls =  MonthlyPayslip.all(:conditions =>["payroll_category_id = :pay_cat AND salary_date >= :start_date AND salary_date <= :end_date", { :start_date => params[:start_date], :end_date => params[:end_date],:pay_cat => params[:payroll_cat] }])
          end
          if params[:payroll_cat] == "All"
            @categories =  MonthlyPayslip.all(:conditions =>["salary_date >= :start_date AND salary_date <= :end_date", { :start_date => params[:start_date], :end_date => params[:end_date] }])
          end
          if params[:payroll_cat] == "gross"
            @categories =  MonthlyPayslip.all(:conditions =>["salary_date >= :start_date AND salary_date <= :end_date", { :start_date => params[:start_date], :end_date => params[:end_date] }])
          end
          if params[:payroll_cat] == "net_salary"
            @gross_salary =  MonthlyPayslip.all(:conditions =>["salary_date >= :start_date AND salary_date <= :end_date", { :start_date => params[:start_date], :end_date => params[:end_date] }])
            @categories = []
            @gross_salary.each do |gross|
              @earning = PayrollCategory.find_by_id_and_status_and_is_deleted(gross.payroll_category_id,true,false).is_deduction
              if @earning == false
                @categories << gross
              end
            end
          end
          if params[:payroll_cat] == "deduction"
            @gross_salary =  MonthlyPayslip.all(:conditions =>["salary_date >= :start_date AND salary_date <= :end_date", { :start_date => params[:start_date], :end_date => params[:end_date] }])
            @categories = []
            @gross_salary.each do |deduction|
              @earning = PayrollCategory.find_by_id_and_status_and_is_deleted(deduction.payroll_category_id,true,false).is_deduction
              if @earning == true
                @categories << deduction
              end
            end
          end
          if params[:payroll_cat] == "individual_earnings"
            @responsibility = IndividualPayslipCategory.all(:conditions =>["salary_date >= ? and salary_date <= ? and is_deduction = ? ",params[:start_date],params[:end_date],false] )
          end
        end
        render :template => "alphena_report/employee_payroll_pdf"
      rescue Exception => e
        Rails.logger.info "Exception in alphena_report_controller, employee_payroll_pdf action"
        Rails.logger.info e
        flash[:notice] = "Sorry, something went wrong. Please inform administration"
        redirect_to :controller => :report, :action => :employee_payroll_category
      end
    end

    def generate_csv_file
      flash[:clear]
      begin
        @payroll_cat = PayrollCategory.find(params[:payroll_cat])
        payroll_csv = FasterCSV.generate({:col_sep => "\t"}) do |csv|
          #header rows
          csv << ["","#{@payroll_cat.name.upcase}","","From",params[:start_date],"To",params[:end_date]]
          # data rows
          csv << ["S/N", "Staff Number", "Staff Name", "Department", "Rank", "Nature of Appointment", "Date of Assumption", "Payroll Category", "Salary Structure", "GL/Level", "Amount"]
          post_data = params[:payroll_cat]
          unless post_data.blank?
            @payrolls =  MonthlyPayslip.all(:conditions =>["payroll_category_id = :pay_cat AND salary_date >= :start_date AND salary_date <= :end_date", { :start_date => params[:start_date], :end_date => params[:end_date],:pay_cat => post_data }])
          end
          total=0
          @ct = 1
          @payrolls.each do |pay_cat|
            @emp = Employee.find(pay_cat.employee_id)
            @emp.employee_additional_details.each do |emp|
              if emp.additional_field.name == "Rank"
                @rank = emp.additional_info
              end
              if emp.additional_field.name == "Nature of Appointment"
                @noa = emp.additional_info
              end
              if emp.additional_field.name == "Salary Structure"
                @salary_structure = emp.additional_info
              end
              if emp.additional_field.name == "GL Level"
                @gl_level = emp.additional_info
              end
            end
            csv << ["#{@ct}","#{@emp.employee_number}","#{@emp.first_name + @emp.last_name}","#{@dept = EmployeeDepartment.find(@emp.employee_department_id).name}",
              "#{@rank}","#{@noa}","#{pay_cat.salary_date}","#{@pay_cat = PayrollCategory.find(pay_cat.payroll_category_id).name}","#{@salary_structure}","#{@gl_level}","#{pay_cat.amount}"]
            total+=pay_cat.amount.to_i
            @ct+=1
          end
          csv << [""]
          csv << ["","","","","","","","","","Grand Total", total]
        end
        send_data(payroll_csv, :type => 'text/csv; charset=utf-8; header=present', :filename => "payroll.csv")
      rescue Exception => e
        Rails.logger.info "Exception in alphena_report_controller, generate_csv_file action"
        Rails.logger.info e
        flash[:notice] = "Sorry, something went wrong. Please inform administration"
        redirect_to :controller => :report, :action => :employee_payroll_category
      end
    end


    def responsibility_csv_file
      flash[:clear]
      begin
        payroll_csv = FasterCSV.generate({:col_sep => "\t"}) do |csv|
          #header rows
          csv << ["","#{params[:payroll_cat].upcase}","","From",params[:start_date],"","To",params[:end_date]]
          # data rows
          csv << [""]
          csv << ["","S/N", "Staff Name", "Department","Salary Date","Rank","Nature of Appointment","Salary Structure","GL Level", "Amount"]
          post_data = params[:payroll_cat]
          unless post_data.blank?
            @responsibility = IndividualPayslipCategory.all(:conditions =>["salary_date >= ? and salary_date <= ? and is_deduction = ? ",params[:start_date],params[:end_date],false] )
            @payrolls =  MonthlyPayslip.all(:conditions =>["payroll_category_id = :pay_cat AND salary_date >= :start_date AND salary_date <= :end_date", { :start_date => params[:start_date], :end_date => params[:end_date],:pay_cat => post_data }])
          end
          total=0
          @sn = 1
          @responsibility.each do |gross|
            @emp = Employee.find(gross.employee_id)
            @emp.employee_additional_details.each do |emp|
              if emp.additional_field.name == "Rank"
                @rank = emp.additional_info
              end
              if emp.additional_field.name == "Nature of Appointment"
                @noa = emp.additional_info
              end
              if emp.additional_field.name == "Salary Structure"
                @salary_structure = emp.additional_info
              end
              if emp.additional_field.name == "GL Level"
                @gl_level = emp.additional_info
              end
            end
            @emp = Employee.find(gross.employee_id)
            csv << ["","#{@sn}","#{@emp.full_name}","#{@emp.employee_department.name}","#{gross.salary_date}", "#{@rank}", "#{@noa}", "#{@salary_structure}", "#{@gl_level}", "#{gross.amount}"]
            total+=gross.amount.to_i
            @sn+=1
          end
          csv << [""]
          csv << ["","","Grand Total","","","","","","", total]
        end
        send_data(payroll_csv, :type => 'text/csv; charset=utf-8; header=present', :filename => "payroll.csv")
      rescue Exception => e
        Rails.logger.info "Exception in alphena_report_controller, generate_csv_file action"
        Rails.logger.info e
        flash[:notice] = "Sorry, something went wrong. Please inform administration"
        redirect_to :controller => :report, :action => :employee_payroll_category
      end
    end

    def individual_deduction_csv_file
      flash[:clear]
      begin
        payroll_csv = FasterCSV.generate({:col_sep => "\t"}) do |csv|
          #header rows
          csv << ["","#{params[:payroll_cat].upcase}","","From",params[:start_date],"","To",params[:end_date]]
          # data rows
          csv << [""]
          csv << ["","S/N", "Staff Name", "Department","Salary Date","Rank","Nature of Appointment","Salary Structure","GL Level", "Amount"]
          post_data = params[:payroll_cat]
          unless post_data.blank?
            @responsibility = IndividualPayslipCategory.all(:conditions =>["salary_date >= ? and salary_date <= ? and is_deduction = ? ",params[:start_date],params[:end_date],true] )    
            @payrolls =  MonthlyPayslip.all(:conditions =>["payroll_category_id = :pay_cat AND salary_date >= :start_date AND salary_date <= :end_date", { :start_date => params[:start_date], :end_date => params[:end_date],:pay_cat => post_data }])
          end
          total=0
          @sn = 1
          @responsibility.each do |gross|
            @emp = Employee.find(gross.employee_id)
            @emp.employee_additional_details.each do |emp|
              if emp.additional_field.name == "Rank"
                @rank = emp.additional_info
              end
              if emp.additional_field.name == "Nature of Appointment"
                @noa = emp.additional_info
              end
              if emp.additional_field.name == "Salary Structure"
                @salary_structure = emp.additional_info
              end
              if emp.additional_field.name == "GL Level"
                @gl_level = emp.additional_info

              end  
            end      
            @emp = Employee.find(gross.employee_id)
            csv << ["","#{@sn}","#{@emp.full_name}","#{@emp.employee_department.name}","#{gross.salary_date}", "#{@rank}", "#{@noa}", "#{@salary_structure}", "#{@gl_level}", "#{gross.amount}"]     
            total+=gross.amount.to_i
            @sn+=1
          end
          csv << [""]
          csv << ["","","Grand Total","","","","","","", total]
        end
        send_data(payroll_csv, :type => 'text/csv; charset=utf-8; header=present', :filename => "payroll.csv")
      rescue Exception => e
        Rails.logger.info "Exception in alphena_report_controller, generate_csv_file action"
        Rails.logger.info e
        flash[:notice] = "Sorry, something went wrong. Please inform administration"
        redirect_to :controller => :report, :action => :employee_payroll_category
      end
    end
    
    def generate_gross_csv_file
      flash[:clear]
      begin
        payroll_csv = FasterCSV.generate({:col_sep => "\t"}) do |csv|
          #header rows
          csv << ["","#{params[:payroll_cat].upcase}","","From",params[:start_date],"To",params[:end_date]]
          # data rows
          csv << ["S/N", "Staff Number", "Staff Name", "Department", "Rank", "Nature of Appointment", "Date of Assumption", "Salary Structure", "GL/Level", "Amount"]
          if params[:payroll_cat] == "All"
            @categories =  MonthlyPayslip.all(:conditions =>["salary_date >= :start_date AND salary_date <= :end_date", { :start_date => params[:start_date], :end_date => params[:end_date] }])
          end
          if params[:payroll_cat] == "gross"
            @categories =  MonthlyPayslip.all(:conditions =>["salary_date >= :start_date AND salary_date <= :end_date", { :start_date => params[:start_date], :end_date => params[:end_date] }])
          end
          if params[:payroll_cat] == "net_salary"
            @gross_salary =  MonthlyPayslip.all(:conditions =>["salary_date >= :start_date AND salary_date <= :end_date", { :start_date => params[:start_date], :end_date => params[:end_date] }])
            @categories = []
            @gross_salary.each do |gross|
              @earning = PayrollCategory.find_by_id_and_status_and_is_deleted(gross.payroll_category_id,true,false).is_deduction
              if @earning == false
                @categories << gross
              end
            end
          end
          if params[:payroll_cat] == "deduction"
            @gross_salary =  MonthlyPayslip.all(:conditions =>["salary_date >= :start_date AND salary_date <= :end_date", { :start_date => params[:start_date], :end_date => params[:end_date] }])
            @categories = []
            @gross_salary.each do |deduction|
              @earning = PayrollCategory.find_by_id_and_status_and_is_deleted(deduction.payroll_category_id,true,false).is_deduction
              if @earning == true
                @categories << deduction
              end
            end
          end
          total=0
          grand_total=0
          @counter = 1
          @ct = 1
          @categories.each do |gross|
            @count = 0
            @emp = Employee.find(gross.employee_id)
            if params[:payroll_cat] == "net_salary"
              @earnings = PayrollCategory.find_all_by_is_deduction_and_status_and_is_deleted(false,true,false)
              @earnings.each do |earning|
                @pay_earning = AlphenaPaygradePayrollCategory.find_by_paygrade_id_and_payroll_category_id(@emp.paygrade_id,earning.id)
                unless @pay_earning.nil?
                  @count+=1
                end
              end
            elsif params[:payroll_cat] == "deduction"
              @deductions = PayrollCategory.find_all_by_is_deduction_and_status_and_is_deleted(true,true,false)
              @deductions.each do |deduction|
                @pay_deduction = AlphenaPaygradePayrollCategory.find_by_paygrade_id_and_payroll_category_id(@emp.paygrade_id,deduction.id)
                unless @pay_deduction.nil?
                  @count+=1
                end
              end
            else
              @count = AlphenaPaygradePayrollCategory.find_all_by_paygrade_id(@emp.paygrade_id).count
            end
            @emp.employee_additional_details.each do |emp|
              if emp.additional_field.name == "Rank"
                @rank = emp.additional_info
              end
              if emp.additional_field.name == "Nature of Appointment"
                @noa = emp.additional_info
              end
              if emp.additional_field.name == "Salary Structure"
                @salary_structure = emp.additional_info
              end
              if emp.additional_field.name == "GL Level"
                @gl_level = emp.additional_info
              end
            end
            if @counter == 1
              @emp = Employee.find(gross.employee_id)
              @emp.employee_additional_details.each do |emp|
                if emp.additional_field.name == "Rank"
                  @rank = emp.additional_info
                end
                if emp.additional_field.name == "Nature of Appointment"
                  @noa = emp.additional_info
                end
                if emp.additional_field.name == "Salary Structure"
                  @salary_structure = emp.additional_info
                end
                if emp.additional_field.name == "GL Level"
                  @gl_level = emp.additional_info
                end
              end
              individual_category = IndividualPayslipCategory.find_by_employee_id_and_salary_date_and_is_deduction(gross.employee_id,gross.salary_date,false)
              total+=gross.amount.to_i + (individual_category.nil? ? 0 : individual_category.amount).to_f
              @counter+=1
            elsif @count >= @counter
              total+=gross.amount.to_i + (individual_category.nil? ? 0 : individual_category.amount).to_f
              if @counter == @count
                @counter = 1
                csv << ["#{@ct}","#{@emp.employee_number}","#{@emp.first_name + @emp.last_name}","#{@dept = EmployeeDepartment.find(@emp.employee_department_id).name}",
                  "#{@rank}","#{@noa}","#{gross.salary_date}","#{@salary_structure}","#{@gl_level}","#{total}"]
                total = 0
                @ct+=1
              else
                @counter+=1
              end
            end
            grand_total+=gross.amount.to_i + (individual_category.nil? ? 0 : individual_category.amount).to_f
          end
          csv << [""]
          csv << ["","","","","","","","","Grand total", grand_total]
        end
        send_data(payroll_csv, :type => 'text/csv; charset=utf-8; header=present', :filename => "category.csv")
      rescue Exception => e
        Rails.logger.info "Exception in alphena_report_controller, generate_gross_csv_file action"
        Rails.logger.info e
        flash[:notice] = "Sorry, something went wrong. Please inform administration"
        redirect_to :controller => :report, :action => :employee_payroll_category
      end
    end

    def cummulative_report
      flash[:clear]
      @departments = EmployeeDepartment.all
      @additional_field_name = AdditionalField.find_all_by_name(["Nature of Appointment","Salary Structure","GL Level"])
      @gross_salary =  MonthlyPayslip.all(:conditions =>["salary_date >= :start_date AND salary_date <= :end_date", { :start_date => params[:start_date], :end_date => params[:end_date] }])
      @categories = []
      @gross_salary.each do |gross|
        @earning = PayrollCategory.find_by_id_and_status_and_is_deleted(gross.payroll_category_id,true,false)
        @earning = @earning.is_deduction unless @earning.nil?
        if @earning == false
          @categories << gross
        end
      end
      @categories.each do |gross|
        @count = 0
        @emp = Employee.find(gross.employee_id)
      end
      begin
        if request.post?
          flash.clear
          unless params[:start_date] > params[:end_date]
            if params[:name][:id] == "All"
              @hash = {}
              @payrolls =  MonthlyPayslip.all(:conditions =>["salary_date >= :start_date AND salary_date <= :end_date", { :start_date => params[:start_date], :end_date => params[:end_date] }])
              @payrolls.each do|payroll|
                dept = payroll.employee.employee_department
                if @hash["#{dept.id}"].nil?
                  @hash["#{dept.id}"] = []
                  @hash["#{dept.id}"] << payroll.id
                else
                  @hash["#{dept.id}"] << payroll.id
                end
              end
            else
              @payslips =  MonthlyPayslip.all(:conditions =>["salary_date >= :start_date AND salary_date <= :end_date", { :start_date => params[:start_date], :end_date => params[:end_date] }])
              unless @payslips.empty?
                @payrolls = []
                @payslips.each do |payslip|
                  @department_id = params[:name][:id]
                  if payslip.employee.employee_department_id == @department_id.to_i
                    @payrolls << payslip
                  end
                end
              end
            end
          else
            flash[:notice] = "#{t('date_range')}"
          end
          unless @emp.nil?
          @emp.employee_additional_details.each do |emp|
            if emp.additional_field.name == "Nature of Appointment"
              @noa = emp.additional_info
            end
            if emp.additional_field.name == "Salary Structure"
              @salary_structure = emp.additional_info
            end
            if emp.additional_field.name == "GL Level"
              @gl_level = emp.additional_info
            end
          end
        end
        end
        render :template => "alphena_report/cummulative_report"
      rescue Exception => e
        Rails.logger.info "Exception in alphena_report_controller, cummulative_report action"
        Rails.logger.info e
        flash[:notice] = "Sorry, something went wrong. Please inform administration"
        redirect_to :controller => :report, :action => :employee_payroll_reports
      end
    end

    def cumulative_pdf
      flash[:clear]
      begin
        @payrolls =  MonthlyPayslip.all(:conditions =>["salary_date >= :start_date AND salary_date <= :end_date", { :start_date => params[:start_date], :end_date => params[:end_date] }])
        render :pdf => "alphena_report/cumulative_pdf",
          :template => "alphena_report/cumulative_pdf",
          :orientation => 'Landscape',
          :header => {:html => { :template=> 'layouts/pdf_header.html.erb'}},
          :footer => {:html => { :template=> 'layouts/pdf_footer.html.erb'}},
          :margin => {    :top=> 34,
          :bottom => 20,
          :left=> 5,
          :right => 5}
      rescue Exception => e
        Rails.logger.info "Exception in alphena_report_controller, cumulative_pdf action"
        Rails.logger.info e
        flash[:notice] = "Sorry, something went wrong. Please inform administration"
        redirect_to :controller => :report, :action => :cummulative_report
      end
    end


    def cumulative_csv_file
      flash[:clear]
      #begin
        @departments = EmployeeDepartment.all
        @payroll_category= PayrollCategory.active
        @overall_earning_categories =[]
        @overall_deduction_categories =[]
        unless @payroll_category.empty?
          @payroll_category.each do |payroll_category|
            if payroll_category.is_deduction==false
              @overall_earning_categories << payroll_category
            else
              @overall_deduction_categories << payroll_category
            end
          end
        end
        @e_name= []
        @d_name = []
        unless @overall_earning_categories.nil?
          @overall_earning_categories.each do |earning_categories|
            @e_name << earning_categories.name
          end
        end
        unless @overall_deduction_categories.nil?
          @overall_deduction_categories.each do |deduction_categories|
            @d_name << deduction_categories.name
          end
        end
        payroll_csv = FasterCSV.generate({:col_sep => "\t"}) do |csv|
          #header rows
          csv << ["","","","","CUMULATIVE","PAYROLL","REPORTS"]
          csv << [""]
          csv << ["","","","From",params[:start_date],"To",params[:end_date]]
          csv << [""]
          # data rows
          csv << ["","","","","","","","","","","","","Arrears","and","Allowances","","","Deductions"]
          csv << [ t('no1'),t('employee_id1'),t('employee_name1'), t('department'), t('position'), t('assumption_date'), t('grade'),t('pay_grade'),t('nature_of_appointment'),t('salary_structure'),t('gl_level')]+@e_name+ [t('total_arrears_and_allowances'),t('gross_salary')] +@d_name+ [t('total_deductions'),t('net_salary')]

          csv << [""]
          csv << [""]
          if params[:dept_id] == "All"
            @hash = {}
            @payrolls =  MonthlyPayslip.all(:conditions =>["salary_date >= :start_date AND salary_date <= :end_date", { :start_date => params[:start_date], :end_date => params[:end_date] }])
            @payrolls.each do|payroll|
              dept = payroll.employee.employee_department
              if @hash["#{dept.id}"].nil?
                @hash["#{dept.id}"] = []
                @hash["#{dept.id}"] << payroll.id
              else
                @hash["#{dept.id}"] << payroll.id
              end
            end
            unless @hash.blank?
              hash_grand_total = {}
              @hash.each do |k,v|
                i=0
                hash_total = {}
                @counter = 1
                @additional_field_name = AdditionalField.find_all_by_name(["Nature of Appointment","Salary Structure","GL Level"])
                v.each do |ids|
                  @mon_payslip =  MonthlyPayslip.find ids.to_i
                  @emp = @mon_payslip.employee
                  @employee_paygrade = AlphenaPaygradePayrollCategory.find_all_by_paygrade_id(@emp.paygrade_id)
                  @count = @employee_paygrade.count
                  @earning_categories = []
                  @deduction_categories = []
                  @employee_paygrade.each do |employee_paygrade|
                    @categories = PayrollCategory.find_by_id_and_status_and_is_deleted(employee_paygrade.payroll_category_id,true,false)
                    unless @categories.nil?
                      if @categories.is_deduction == false
                        @earning_categories << employee_paygrade
                      else
                        @deduction_categories << employee_paygrade
                      end
                    end
                    unless @emp.nil?
                      @emp.employee_additional_details.each do |emp|
                        if emp.additional_field.name == "Nature of Appointment"
                          @noa = emp.additional_info.nil? ? "" :  emp.additional_info
                        end
                        if emp.additional_field.name == "Salary Structure"
                          @salary_structure = emp.additional_info.nil? ? "" :  emp.additional_info
                        end
                        if emp.additional_field.name == "GL Level"
                          @gl_level = emp.additional_info.nil? ? "" :  emp.additional_info
                        end
                      end
                    else
                      @noa,@salary_structure,@gl_level = ""
                    end
                  end
                  if @counter == 1
                    @grades = @emp.employee_grade
                    unless @overall_earning_categories.nil?
                      @earning_total = 0
                      @e_amount =[]
                      @overall_earning_categories.each do |overall_earning_category|
                        @earning = MonthlyPayslip.find_by_employee_id_and_salary_date_and_payroll_category_id(@emp.id ,@mon_payslip.salary_date,overall_earning_category.id)
                        unless @earning.nil?
                          @e_amount << @earning.amount
                          #@earning_total += @earning.amount.to_f
                          if hash_total["#{overall_earning_category.id }"].nil?
                            hash_total["#{overall_earning_category.id }"] = @earning.amount.to_f
                          else
                            hash_total["#{overall_earning_category.id }"] += @earning.amount.to_f
                          end
                          @earning.amount == 0 ? "" :   @earning.amount
                          @earning_total += @earning.amount.to_f
                        else
                          @e_amount << ""
                        end
                      end
                    end
                    individual_earning_payslip = IndividualPayslipCategory.all(:conditions =>["salary_date >= ? and salary_date <= ? and employee_id = ? and is_deduction = ?",params[:start_date],params[:end_date],@emp.id,false] )
                    # unless individual_earning_payslip.empty?
                    #   responsibility_total = 0
                    #   individual_earning_payslip.each do |payslip|
                    #     responsibility_total += payslip.amount.to_f
                    #   end
                    #   if hash_total["responsiblity"].nil?
                    #     hash_total["responsiblity"] =  responsibility_total
                    #   else
                    #     # hash_total["responsiblity"] += responsibility_total
                    #   end
                    #   # @e_amount << responsibility_total
                    #   # @earning_total +=responsibility_total
                    # else
                    #   # @e_amount << ""
                    # end
                    if hash_total["@earning_total"].nil?
                      hash_total["@earning_total"] =  @earning_total
                    else
                      hash_total["@earning_total"] +=  @earning_total
                    end
                    @earning_total == 0 ? "" :  @earning_total
                    unless @overall_deduction_categories.nil?
                      @deduction_total = 0
                      @d_amount = []
                      @overall_deduction_categories.each do |overall_deduction_category|
                        @deductions = MonthlyPayslip.find_by_employee_id_and_salary_date_and_payroll_category_id(@emp.id ,@mon_payslip.salary_date,overall_deduction_category.id)
                        unless @deductions.nil?
                          @d_amount << @deductions.amount
                          #@deduction_total += @deductions.amount.to_f
                          if hash_total["#{overall_deduction_category.id }"].nil?
                            hash_total["#{overall_deduction_category.id }"] = @deductions.amount.to_f
                          else
                            hash_total["#{overall_deduction_category.id }"] += @deductions.amount.to_f
                          end
                          @deductions.amount == 0 ? "" :   @deductions.amount
                          @deduction_total += @deductions.amount.to_f
                        else
                          @d_amount << ""
                        end
                      end
                    end
                    if hash_total["@deduction_total"].nil?
                      hash_total["@deduction_total"] =  @deduction_total
                    else
                      hash_total["@deduction_total"] +=  @deduction_total
                    end
                    @deduction_total == 0 ? "" :  @deduction_total
                    @net_salary = @earning_total-@deduction_total
                    if hash_total["@net_salary"].nil?
                      hash_total["@net_salary"] =  @net_salary
                    else
                      hash_total["@net_salary"] +=  @net_salary
                    end
                    @net_salary == 0 ? "" : @net_salary
                    @gross = @earning_total+@deduction_total
                    if hash_total["@gross"].nil?
                      hash_total["@gross"] =  @gross
                    else
                      hash_total["@gross"] +=  @gross
                    end
                    @gross == 0 ? "" : @gross
                    csv << [ i+=1,@emp.employee_number, "#{@emp.first_name} #{@emp.last_name}",
                      @dept = EmployeeDepartment.find(@emp.employee_department_id).name,
                      @emp.employee_position, @emp.joining_date, "#{@grades.name unless @grades.nil?}",
                      AlphenaPaygrade.find(@emp.paygrade_id).name,"#{@noa}","#{@salary_structure}","#{@gl_level}"]+@e_amount+[@earning_total,@gross]+@d_amount+[@deduction_total,@net_salary]
                    # unless @earning_categories.nil?
                    #   earning_total = 0
                    #   @earning_categories.each do |earning_categories|
                    #     csv << ["","","","","","","","","","","", earning_categories.amount]
                    #     earning_total = earning_total+earning_categories.amount
                    #   end
                    # end
                    # unless @deduction_categories.nil?
                    #   deduction_total = 0
                    #   @deduction_categories.each do |deduction_categories|
                    #     csv << ["","","","","","","","","","","", deduction_categories.amount]
                    #     deduction_total = deduction_total+deduction_categories.amount
                    #   end
                    # end
                    @counter+=1
                  elsif @count >= @counter
                    if @counter == @count
                      @counter = 1
                    else
                      @counter+=1
                    end
                  end
                end #payroll end
                unless @payroll_category.nil?
                  grand_total = []
                  if @overall_earning_categories.present?
                    @overall_earning_categories.each do |overall_earning_category|
                      if hash_grand_total["#{overall_earning_category.id}"].nil?
                      hash_grand_total["#{overall_earning_category.id}"] = hash_total["#{overall_earning_category.id}"].to_f
                      else
                         hash_grand_total["#{overall_earning_category.id}"] += hash_total["#{overall_earning_category.id}"].to_f
                      end
                    grand_total << hash_total["#{overall_earning_category.id}"]
                    end
                     if hash_grand_total["@earning_total"].nil?
                      hash_grand_total["@earning_total"] = hash_total["@earning_total"].to_f
                      else
                         hash_grand_total["@earning_total"] += hash_total["@earning_total"].to_f
                      end
                      if hash_grand_total["responsiblity"].nil?
                      hash_grand_total["responsiblity"] = hash_total["responsiblity"].to_f
                      else
                         # hash_grand_total["responsiblity"] += hash_total["responsiblity"].to_f
                      end
                    # grand_total << hash_total["responsiblity"]
                    grand_total << hash_total["@earning_total"]
                  end
                   if hash_grand_total["@gross"].nil?
                      hash_grand_total["@gross"] = hash_total["@gross"].to_f
                      else
                         hash_grand_total["@gross"] += hash_total["@gross"].to_f
                      end
                  grand_total << hash_total["@gross"]
                  if @overall_deduction_categories.present?
                    @overall_deduction_categories.each do |overall_deduction_category|
                      if hash_grand_total["#{overall_deduction_category.id}"].nil?
                      hash_grand_total["#{overall_deduction_category.id}"] = hash_total["#{overall_deduction_category.id}"].to_f
                      else
                         hash_grand_total["#{overall_deduction_category.id}"] += hash_total["#{overall_deduction_category.id}"].to_f
                      end
                      grand_total << hash_total["#{overall_deduction_category.id}"]
                    end
                     if hash_grand_total["@deduction_total"].nil?
                      hash_grand_total["@deduction_total"] = hash_total["@deduction_total"].to_f
                      else
                         hash_grand_total["@deduction_total"] += hash_total["@deduction_total"].to_f
                      end
                    grand_total << hash_total["@deduction_total"]
                  end
                  if hash_grand_total["@net_salary"].nil?
                      hash_grand_total["@net_salary"] = hash_total["@net_salary"].to_f
                      else
                         hash_grand_total["@net_salary"] += hash_total["@net_salary"].to_f
                      end
                  grand_total << hash_total["@net_salary"]
                end
                csv <<  ["#{t('total')}","","","","","","","","","",""] + grand_total
                csv << [""]
              end
               unless @payroll_category.nil?
                all_grand_total = []
                if @overall_earning_categories.present?
                  @overall_earning_categories.each do |overall_earning_category|
                    all_grand_total << hash_grand_total["#{overall_earning_category.id}"]
                  end
                  # all_grand_total <<  hash_grand_total["responsiblity"]
                  all_grand_total<<  hash_grand_total["@earning_total"]
                end
                all_grand_total <<  hash_grand_total["@gross"]
                if @overall_deduction_categories.present?
                  @overall_deduction_categories.each do |overall_deduction_category|
                    all_grand_total <<  hash_grand_total["#{overall_deduction_category.id}"]
                  end
                  all_grand_total <<  hash_grand_total["@deduction_total"]
                end
                all_grand_total<<  hash_grand_total["@net_salary"]
              end
              # csv << [""]
              csv <<  ["#{t('grand_total')}","","","","","","","","","",""] + all_grand_total
            end #hash unless end


          else
            @payslips =  MonthlyPayslip.all(:conditions =>["salary_date >= :start_date AND salary_date <= :end_date", { :start_date => params[:start_date], :end_date => params[:end_date] }])
            unless @payslips.empty?
              @payrolls = []
              @payslips.each do |payslip|
                @department_id = params[:dept_id]
                if payslip.employee.employee_department_id == @department_id.to_i
                  @payrolls << payslip
                end
              end
              i=0
              hash_total = {}
              @counter = 1
              @additional_field_name = AdditionalField.find_all_by_name(["Nature of Appointment","Salary Structure","GL Level"])
              @payrolls.each do |gross|
                @emp = Employee.find(gross.employee_id)
                @employee_paygrade = AlphenaPaygradePayrollCategory.find_all_by_paygrade_id(@emp.paygrade_id)
                @count = @employee_paygrade.count
                @earning_categories = []
                @deduction_categories = []
                @employee_paygrade.each do |employee_paygrade|
                  @categories = PayrollCategory.find_by_id_and_status_and_is_deleted(employee_paygrade.payroll_category_id,true,false)
                  unless @categories.nil?
                    if @categories.is_deduction == false
                      @earning_categories << employee_paygrade
                    else
                      @deduction_categories << employee_paygrade
                    end
                  end
                  unless @emp.nil?
                    @emp.employee_additional_details.each do |emp|
                      if emp.additional_field.name == "Nature of Appointment"
                        @noa = emp.additional_info.nil? ? "" :  emp.additional_info
                      end
                      if emp.additional_field.name == "Salary Structure"
                        @salary_structure = emp.additional_info.nil? ? "" :  emp.additional_info
                      end
                      if emp.additional_field.name == "GL Level"
                        @gl_level = emp.additional_info.nil? ? "" :  emp.additional_info
                      end
                    end
                  else
                    @noa,@salary_structure,@gl_level = ""
                  end
                end
                if @counter == 1
                  @grades = @emp.employee_grade
                  unless @overall_earning_categories.nil?
                    @earning_total = 0
                    @e_amount =[]
                    @overall_earning_categories.each do |overall_earning_category|
                      @earning = MonthlyPayslip.find_by_employee_id_and_salary_date_and_payroll_category_id(@emp.id ,gross.salary_date,overall_earning_category.id)
                      unless @earning.nil?
                        @e_amount << @earning.amount
                        #@earning_total += @earning.amount.to_f
                        if hash_total["#{overall_earning_category.id }"].nil?
                          hash_total["#{overall_earning_category.id }"] = @earning.amount.to_f
                        else
                          hash_total["#{overall_earning_category.id }"] += @earning.amount.to_f
                        end
                        @earning.amount == 0 ? "" :   @earning.amount
                        @earning_total += @earning.amount.to_f
                      else
                        @e_amount << ""
                      end
                    end
                  end
                  individual_earning_payslip = IndividualPayslipCategory.all(:conditions =>["salary_date >= ? and salary_date <= ? and employee_id = ? and is_deduction = ?",params[:start_date],params[:end_date],@emp.id,false] )
                  # unless individual_earning_payslip.empty?
                  #   responsibility_total = 0
                  #   individual_earning_payslip.each do |payslip|
                  #     responsibility_total += payslip.amount.to_f
                  #   end
                  #   if hash_total["responsiblity"].nil?
                  #     hash_total["responsiblity"] =  responsibility_total
                  #   else
                  #     hash_total["responsiblity"] += responsibility_total
                  #   end
                  #   @e_amount << responsibility_total
                  #   @earning_total +=responsibility_total
                  # else
                  #   @e_amount << ""
                  # end
                  if hash_total["@earning_total"].nil?
                    hash_total["@earning_total"] =  @earning_total
                  else
                    hash_total["@earning_total"] +=  @earning_total
                  end
                  @earning_total == 0 ? "" :  @earning_total
                  unless @overall_deduction_categories.nil?
                    @deduction_total = 0
                    @d_amount = []
                    @overall_deduction_categories.each do |overall_deduction_category|
                      @deductions = MonthlyPayslip.find_by_employee_id_and_salary_date_and_payroll_category_id(@emp.id ,gross.salary_date,overall_deduction_category.id)
                      unless @deductions.nil?
                        @d_amount << @deductions.amount
                        #@deduction_total += @deductions.amount.to_f
                        if hash_total["#{overall_deduction_category.id }"].nil?
                          hash_total["#{overall_deduction_category.id }"] = @deductions.amount.to_f
                        else
                          hash_total["#{overall_deduction_category.id }"] += @deductions.amount.to_f
                        end
                        @deductions.amount == 0 ? "" :   @deductions.amount
                        @deduction_total += @deductions.amount.to_f
                      else
                        @d_amount << ""
                      end
                    end
                  end
                  if hash_total["@deduction_total"].nil?
                    hash_total["@deduction_total"] =  @deduction_total
                  else
                    hash_total["@deduction_total"] +=  @deduction_total
                  end
                  @deduction_total == 0 ? "" :  @deduction_total
                  @net_salary = @earning_total-@deduction_total
                  if hash_total["@net_salary"].nil?
                    hash_total["@net_salary"] =  @net_salary
                  else
                    hash_total["@net_salary"] +=  @net_salary
                  end
                  @net_salary == 0 ? "" : @net_salary
                  @gross = @earning_total+@deduction_total
                  if hash_total["@gross"].nil?
                    hash_total["@gross"] =  @gross
                  else
                    hash_total["@gross"] +=  @gross
                  end
                  @gross == 0 ? "" : @gross
                  csv << [ i+=1,@emp.employee_number, "#{@emp.first_name} #{@emp.last_name}",
                    @dept = EmployeeDepartment.find(@emp.employee_department_id).name,
                    @emp.employee_position, @emp.joining_date, "#{@grades.name unless @grades.nil?}",
                    AlphenaPaygrade.find(@emp.paygrade_id).name,"#{@noa}","#{@salary_structure}","#{@gl_level}"]+@e_amount+[@earning_total,@gross]+@d_amount+[@deduction_total,@net_salary]
                  # unless @earning_categories.nil?
                  #   earning_total = 0
                  #   @earning_categories.each do |earning_categories|
                  #     csv << ["","","","","","","","","","","", earning_categories.amount]
                  #     earning_total = earning_total+earning_categories.amount
                  #   end
                  # end
                  # unless @deduction_categories.nil?
                  #   deduction_total = 0
                  #   @deduction_categories.each do |deduction_categories|
                  #     csv << ["","","","","","","","","","","", deduction_categories.amount]
                  #     deduction_total = deduction_total+deduction_categories.amount
                  #   end
                  # end
                  @counter+=1
                elsif @count >= @counter
                  if @counter == @count
                    @counter = 1
                  else
                    @counter+=1
                  end
                end
              end
              unless @payroll_category.nil?
                grand_total = []
                if @overall_earning_categories.present?
                  @overall_earning_categories.each do |overall_earning_category|
                    grand_total << hash_total["#{overall_earning_category.id}"]
                  end
                  # grand_total << hash_total["responsiblity"]
                  grand_total << hash_total["@earning_total"]
                end
                grand_total << hash_total["@gross"]
                if @overall_deduction_categories.present?
                  @overall_deduction_categories.each do |overall_deduction_category|
                    grand_total << hash_total["#{overall_deduction_category.id}"]
                  end
                  grand_total << hash_total["@deduction_total"]
                end
                grand_total << hash_total["@net_salary"]
              end
              csv << [""]
              csv <<  ["","","","","","","","","","","#{t('grand_total')}"] + grand_total

            end #payslips unless end
          end #if else end

        end #csv end
        send_data(payroll_csv, :type => 'text/csv; charset=utf-8; header=present', :filename => "cumulative.csv")
     # rescue Exception => e
      #  Rails.logger.info "Exception in alphena_report_controller, cumulative_csv_file action"
       # Rails.logger.info e
       # flash[:notice] = "Sorry, something went wrong. Please inform administration"
      #  redirect_to :controller => :report, :action => :cummulative_report
     # end
    end
    # def payrolls_details_csv_file
    #       payroll_csv = FasterCSV.generate({:col_sep => "\t"}) do |csv|
    #         #header rows
    #         csv << ["","","","#{t('employee_text')} #{t('payroll_text')} #{t('details')}"]
    #         # data rows
    #         csv << [ t('no_text'), t('name'), t('employee_id'),t('department'),t('payroll_category'), t('amount'),]
    #         @employees= Employee.paginate(:select=>"first_name,middle_name,last_name,employees.id,employee_departments.name as department_name,count(employee_salary_structures.id) as emp_sub_count,employee_number",:joins=>[:employee_salary_structures,:employee_department],:group=>"employees.id",:page=>params[:page],:per_page=>20 ,:order=>'first_name ASC')
    #         emp_ids=@employees.collect(&:id)
    #         @payroll=EmployeeSalaryStructure.all(:select=>"employee_id,amount,payroll_categories.name,payroll_categories.is_deduction",:joins=>[:payroll_category],:conditions=>["payroll_categories.status=? and employee_id IN (?)",true,emp_ids],:order=>'name ASC').group_by(&:employee_id)
    #         i = 0
    #         @employees.each do |e|
    #         csv << [  i+=1,e.full_name,e.employee_number,e.department_name]
    #         payroll=@payroll[e.id]
    #         total=0.to_f
    #         payroll.each do |p|
    #           if p.is_deduction=="1"
    #             csv << ["","","","",p.name,p.amount]
    #             total -= p.amount.to_f
    #           else
    #             csv << ["","","","",p.name,p.amount]
    #             total += p.amount.to_f
    #           end
    #         end
    #             csv << ["","","","",t('total'),total]
    #       end
    #       end
    #       send_data(payroll_csv, :type => 'text/csv; charset=utf-8; header=present', :filename => "employees_payroll_details.csv")
    #     end
    def monthly_wise_payslip
      flash[:clear]
      begin
        if request.post?
          @e_payslip = {}
          @d_payslip = {}

          flash.clear
          unless params[:start_date] > params[:end_date]
            @monthly_payslip  =  MonthlyPayslip.all(:conditions =>["salary_date >= :start_date AND salary_date <= :end_date", { :start_date => params[:start_date], :end_date => params[:end_date] }])
            @monthly_payslip.each do |payslip|
              name = Costcode.find_by_payroll_category_id(payslip.payroll_category_id)
              unless name.nil?
                if payslip.payroll_category.is_deduction==true
                  if @d_payslip["#{name.id}-#{payslip.payroll_category.name}"].nil?
                    @d_payslip["#{name.id}-#{payslip.payroll_category.name}"] = payslip.amount.to_f
                  else
                    @d_payslip["#{name.id}-#{payslip.payroll_category.name}"] += payslip.amount.to_f
                  end
                else
                  if @e_payslip["#{name.id}-#{payslip.payroll_category.name}"].nil?
                    @e_payslip["#{name.id}-#{payslip.payroll_category.name}"] = payslip.amount.to_f
                  else
                    @e_payslip["#{name.id}-#{payslip.payroll_category.name}"] += payslip.amount.to_f
                  end
                end
              end

            end
          else
            flash[:notice] = "#{t('date_range')}"
          end
        end
        render :template => "alphena_report/monthly_wise_payslip"
      rescue Exception => e
        Rails.logger.info "Exception in alphena_report_controller, monthly_wise_payslip action"
        Rails.logger.info e
        flash[:notice] = "Sorry, something went wrong. Please inform administration"
        redirect_to :controller => :report, :action => :employee_payroll_reports
      end
    end

    # def monthly_wise_payslip_pdf
    #   flash[:clear]
    #   begin
    #  @payrolls =  MonthlyPayslip.all(:conditions =>["salary_date >= :start_date AND salary_date <= :end_date", { :start_date => params[:start_date], :end_date => params[:end_date] }])
    #   render :pdf => "alphena_report/monthly_wise_payslip_pdf",
    #     :template => "alphena_report/monthly_wise_payslip_pdf",
    #     :orientation => 'Landscape',
    #     :header => {:html => { :template=> 'layouts/pdf_header.html.erb'}},
    #     :footer => {:html => { :template=> 'layouts/pdf_footer.html.erb'}},
    #     :margin => {    :top=> 34,
    #     :bottom => 20,
    #     :left=> 5,
    #     :right => 5}
    #     rescue Exception => e
    #     Rails.logger.info "Exception in alphena_report_controller, monthly_wise_payslip_pdf action"
    #     Rails.logger.info e
    #     flash[:notice] = "Sorry, something went wrong. Please inform administration"
    #     redirect_to :controller => :report, :action => :monthly_wise_payslip
    #   end
    # end

    def monthly_wise_payslip_csv_file
      flash[:clear]
      earning_total=0
      deduction_total=0
      # begin
      payroll_csv = FasterCSV.generate({:col_sep => "\t"}) do |csv|
        #header rows
        @monthly_payslip  =  MonthlyPayslip.all(:conditions =>["salary_date >= :start_date AND salary_date <= :end_date", { :start_date => params[:start_date], :end_date => params[:end_date] }])
        csv << ["","","","","MONTHLY-WISE","PAYSLIP REPORTS"]
        csv << [""]
        csv << ["","","","From    ",params[:start_date],"To",params[:end_date]]
        csv << [""]
        # data rows
        csv << ["", t('cost_code'),t('description'), t('debit'), t('credit')]

        params[:earnings].each do |key,value|
          cost_code_name = key.split("-")
          code = Costcode.find(cost_code_name[0].to_i).cost_code_name
          earning_total+=value.to_f
          csv << ["","#{code}","#{cost_code_name[1]}","#{value}","-"]
        end
        params[:deduction].each do |key,value|
          cost_code_name = key.split("-")
          code = Costcode.find(cost_code_name[0].to_i).cost_code_name
          deduction_total+=value.to_f
          csv << ["","#{code}","#{cost_code_name[1]}","-","#{value}"]
        end
        @emp=@monthly_payslip.map {|p| p.employee_id}
        @emp.uniq.count
        csv << [""]
        csv << ["","","Gross Total",earning_total,deduction_total]
        csv << [""]
        csv << [""]
        csv << ["","","Total Arrears and Allowances",earning_total]
        csv << ["","","Total Deductions",deduction_total]
        csv << ["","","Total Staff",@emp.uniq.count]
      end
      send_data(payroll_csv, :type => 'text/csv; charset=utf-8; header=present', :filename => "monthly_wise_payslip.csv")
      # rescue Exception => e
      #   Rails.logger.info "Exception in alphena_report_controller, monthly_wise_payslip_csv_file action"
      #   Rails.logger.info e
      #   flash[:notice] = "Sorry, something went wrong. Please inform administration"
      #   redirect_to :controller => :report, :action => :monthly_wise_payslip
      # end

    end

    def employees_with_tmpl
      @additional_field_name = AdditionalField.find_all_by_name(["PenCom PIN","Pencom Administrator","L G A"])
      @dpt_count=EmployeeDepartment.active.count
      @count=Employee.all(:select => "count(IF(employees.gender Like '%m%' ,1,NULL)) as male_count, count(IF(employees.gender LIKE '%f%',1,NULL)) as female_count")
      @sort_order=params[:sort_order]
      if @sort_order.nil?
        @employees=Employee.paginate(:select => "employees.id,employees.first_name,employees.middle_name,employees.last_name,employee_number,joining_date,date_of_birth,home_state,mobile_phone,employees.email,qualification,marital_status,home_address_line1,office_address_line1,employee_departments.name as department_name,employee_positions.name as emp_position,gender , employees.id as emp_id,users.first_name as manager_first_name ,users.last_name as manager_last_name", :joins => "INNER JOIN `employee_departments` ON `employee_departments`.id = `employees`.employee_department_id INNER JOIN `employee_positions` ON `employee_positions`.id = `employees`.employee_position_id LEFT OUTER JOIN `users` ON `users`.id = `employees`.reporting_manager_id", :per_page => 20, :page => params[:page], :order => 'first_name ASC')
      else
        @employees=Employee.paginate(:select => "employees.id,employees.first_name,employees.middle_name,employees.last_name,employee_number,joining_date,date_of_birth,home_state,mobile_phone,employees.email,qualification,marital_status,home_address_line1,office_address_line1,employee_departments.name as department_name,employee_positions.name as emp_position,gender , employees.id as emp_id,users.first_name as manager_first_name ,users.last_name as manager_last_name", :joins => "INNER JOIN `employee_departments` ON `employee_departments`.id = `employees`.employee_department_id INNER JOIN `employee_positions` ON `employee_positions`.id = `employees`.employee_position_id LEFT OUTER JOIN `users` ON `users`.id = `employees`.reporting_manager_id", :per_page => 20, :page => params[:page], :order => @sort_order)
      end
      if request.xhr?
        render :update do |page|
          page.replace_html "information", :partial => "alphena_report/employees_list"
        end
      else
        render :template => "alphena_report/employees_with_tmpl"
      end
    end

    def alphena_employee_csv_list
      @additional_field_name = AdditionalField.find_all_by_name(["PenCom PIN","Pencom Administrator","L G A"])
      payroll_csv = FasterCSV.generate({:col_sep => "\t"}) do |csv|
        csv << ["#{t('no_text')}","#{t('name')}","#{t('employee_id')}","#{t('date_of_birth')}","#{t('date_of_exit')}","#{t('joining_date')}","#{t('department')}","#{t('responsibility')}",
          "#{t('gender')}","#{t('office_state')}","#{t('phone_no')}","#{t('email')}","#{t('qualification')}","#{t('matrial_status')}","#{t('contact_address')}","#{t('residential_address')}","#{t('pencom_pin')}","#{t('pencom_administrator')}","#{t('lga')}"]
        @employees=Employee.all(:select => "employees.id,employees.first_name,employees.middle_name,employees.last_name,employee_number,joining_date,date_of_birth,home_state,mobile_phone,employees.email,qualification,marital_status,home_address_line1,office_address_line1,employee_departments.name as department_name,employee_positions.name as emp_position,gender , employees.id as emp_id,users.first_name as manager_first_name ,users.last_name as manager_last_name", :joins => "INNER JOIN `employee_departments` ON `employee_departments`.id = `employees`.employee_department_id INNER JOIN `employee_positions` ON `employee_positions`.id = `employees`.employee_position_id LEFT OUTER JOIN `users` ON `users`.id = `employees`.reporting_manager_id")
        @employees.each_with_index do |e, i|
          additional_field_values = []
          @additional_field_name.each do |additional_field|
            @ead = EmployeeAdditionalDetail.find_by_employee_id_and_additional_field_id(e.id, additional_field.id)
            if @ead.nil?
              additional_field_values << ""
            else
              additional_field_values <<  @ead.additional_info
            end

          end
          csv << ["#{i+1}","#{e.full_name}","#{e.employee_number}","#{e.date_of_birth}","",
            "#{e.joining_date}","#{e.department_name}","#{e.emp_position}","#{e.gender}","#{e.home_state}","#{e.mobile_phone}",
            "#{e.email}","#{e.qualification}","#{e.marital_status}","#{e.office_address_line1}","#{e.home_address_line1}"] + additional_field_values
        end
      end
      send_data(payroll_csv, :type => 'text/csv; charset=utf-8; header=present', :filename => "employee.csv")
    end

    def after_employee_created
      additional_fields = ["PenCom PIN","Pencom Administrator","L G A","Rank","Nature of Appointment","Salary Structure","GL Level"]
      additional_fields.each do |field|
        @employee_additional_field = AdditionalField.find_by_name(field)
        if @employee_additional_field.nil?
          @all_details = AdditionalField.first(:order=>"priority DESC")
          @employee_additional_field_1 = AdditionalField.new
          priority = 1
          if @all_details.present?
            last_priority = @all_details.priority
            priority = last_priority + 1
          end
          @employee_additional_field = AdditionalField.new
          @additional_field_option = @employee_additional_field.additional_field_options.build
          @employee_additional_field.name = field
          @employee_additional_field.status = true
          @employee_additional_field.input_type = "text"
          @employee_additional_field.is_mandatory = false
          @employee_additional_field.priority = priority
          @employee_additional_field.save
          flash[:notice] = "Created Sucessfully"
        else
          flash[:notice] = "Fields Already Exists"
        end
      end

      redirect_to :controller => :report, :action => :employee_payroll_reports
    end

  end

end
