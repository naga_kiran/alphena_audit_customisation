# AlphenaAuditCustomisation
require 'dispatcher'
require 'overides/alphena_payroll_controller'
require 'overides/alphena_employee_controller'
require 'fastercsv'
require 'overides/alphena_user_controller'
require 'overides/alphena_payroll_controller'
require 'overides/alphena_report_controller'
require 'overides/alphena_application_controller'
require 'overides/alphena_finance_controller'
require 'overides/employee'

module AlphenaAuditCustomisation

  def self.attach_overrides
    Dispatcher.to_prepare  do
      ::User.send :has_paper_trail 
      ::Employee.send :has_paper_trail
      ::TimetableEntry.send :has_paper_trail
      ::Student.send :has_paper_trail
      ::IndividualPayslipCategory.send :has_paper_trail
      ::Exam.send :has_paper_trail
      ::ExamGroup.send :has_paper_trail
      ::Course.send :has_paper_trail
      ::Batch.send :has_paper_trail
      ::Timetable.send :has_paper_trail
      ::FinanceFeeCategory.send :has_paper_trail
      ::FinanceFeeParticular.send :has_paper_trail
      ::FinanceFeeCollection.send :has_paper_trail
      ::CancelledFinanceTransaction.send :has_paper_trail
      ::FeeRefund.send :has_paper_trail
      ::FinanceTransaction.send :has_paper_trail
      ::MonthlyPayslip.send :has_paper_trail
      ::PayrollCategory.send :has_one,:costcode
      ::PayrollController.instance_eval { include AlphenaPayrollController }
      ::EmployeeController.instance_eval { include AlphenaEmployeeController }
      ::UserController.instance_eval { include AlphenaUserController }
      ::ReportController.instance_eval { include AlphenaReportController }
      ::ApplicationController.instance_eval { include AlphenaApplicationController }
      ::FinanceController.instance_eval {include AlphenaFinanceController }
      ::Employee.instance_eval { include EmployeeAux }
      ::Employee.send :extend, EmployeeDepartmentPayroll
    end
  end
end