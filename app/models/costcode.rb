class Costcode < ActiveRecord::Base
	belongs_to  :payroll_category
	validates_presence_of :cost_code_name
    validates_uniqueness_of :cost_code_name
end
