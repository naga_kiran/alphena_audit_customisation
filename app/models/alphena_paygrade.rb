class AlphenaPaygrade < ActiveRecord::Base
  has_paper_trail :on =>  [:update, :create]
  validates_presence_of :name
  validates_uniqueness_of :name
end
