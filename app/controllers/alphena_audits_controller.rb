class AlphenaAuditsController < ApplicationController
  filter_access_to :all
  def alphena_audit_log
    flash[:clear]
     begin
    @events = []
    @modules = Version.find(:all,:conditions => ["item_type != ?", "User"]).map{|p|p.item_type}.uniq
    @audit_details = Version.find_all_by_created_at(Date.today.beginning_of_day..Date.today.end_of_day,:conditions => ["item_type != ?", "User"]).paginate(:page => params[:page], :per_page => 10)
    if request.xhr?
      @start_date = params[:audit_date].to_date
      @end_date = Time.now.strftime("%Y-%m-%d")
      @model_name = params[:module][:module_name]
      @action_name = params[:event][:event_name]
      @user_name = params[:user_name]
      if !@model_name.blank? and !@action_name.blank? and !@user_name.blank?
        @user = Employee.find_by_employee_number(@user_name)
        unless @user.nil?
          @audit_details =   Version.find(:all, :conditions =>["item_type = ? AND whodunnit = ? AND event = ? AND date(created_at) BETWEEN ? AND ?",@model_name,@user.id,@action_name, @start_date,@end_date]).paginate(:page => params[:page], :per_page => 10)
        end
      elsif !@model_name.blank? and !@action_name.blank? and @user_name.blank?
        @audit_details =   Version.find(:all, :conditions =>["item_type = ? AND event = ? AND date(created_at) BETWEEN ? AND ?",@model_name,@action_name, @start_date,@end_date]).paginate(:page => params[:page], :per_page => 10)
      elsif !@user_name.blank? and @model_name.blank? and @action_name.blank?
        @user = Employee.find_by_employee_number(@user_name)
        unless @user.nil?
          @audit_details =   Version.find(:all, :conditions =>["item_type != ? AND whodunnit = ?  AND date(created_at) BETWEEN ? AND ?","User",@user.id, @start_date,@end_date]).paginate(:page => params[:page], :per_page => 10)
        end
      end
      render :update do |page|
        page.replace_html "information", :partial => "alphena_audit_log"
      end
    end
    rescue Exception => e
      Rails.logger.info "Exception in alphena audits controller, alphena audit log action"
      Rails.logger.info e
      flash[:notice] = "Sorry, something went wrong. Please inform administration"
      redirect_to :controller => :alphena_audits, :action => :alphena_audit_links
    end
  end

  def update_module_event
     flash[:clear]
    begin
    @events = Version.find_all_by_item_type(params[:module_name]).map{|p| p.event}.uniq
    render :update do |page|
      page.replace_html 'events', :partial => 'events', :object => @events
    end
     rescue Exception => e
      Rails.logger.info "Exception in alphena audits controller, update module event action"
      Rails.logger.info e
      flash[:notice] = "Sorry, something went wrong. Please inform administration"
      redirect_to :controller => :alphena_audits, :action => :alphena_audit_links
    end
  end

  def audit_link
    render :template => "alphena_audits/alphena_audit_links"
  end

  def alphena_activity
      @events = []
     @audit_details = Version.find_all_by_created_at(Date.today.beginning_of_day..Date.today.end_of_day,:conditions => ["item_type != ?", "User"]).paginate(:page => params[:page], :per_page => 10)
     if request.xhr?
         @start_date = params[:audit_date]
         @end_date = Time.now
          @action_name = params[:event][:event_name]
          @audit_details =   Version.find(:all, :conditions =>["item_type = ? AND event = ? AND date(created_at) BETWEEN ? AND ?",@action_name, @start_date,@end_date]).map{|p| p.action_name}.uniq
         @events = Version.find_by_item_type_and_event(:item_type => params[:item_type],:event => params[:event])
     end
  end

  def alphena_user_audit
     flash[:clear]
     begin
    if request.xhr?
      @inspect_control = false
      @start_date = params[:audit_date].to_date
      @end_date = Time.now.strftime("%Y-%m-%d")
      if params[:search][:search_user] == "student"
        @user_ids = Student.find(:all,:conditions => ["user_id != ? AND  batch_id = ?", "nil",params[:batch_id]]).map{|p| p.user_id}
       @user_details = Version.find(:all, :conditions =>["item_type = ? AND event = ? AND item_id = ? AND date(created_at) BETWEEN ? AND ?","User","update",@user_ids,@start_date,@end_date])
      elsif params[:search][:search_user] == "employee"
        @user_ids = Employee.find(:all,:conditions => ["user_id != ? AND  employee_department_id = ?", "nil",params[:employee_department_id]]).map{|p| p.user_id}
      elsif params[:search][:search_user] == "user_name"
        @user_data = User.find_by_username(params[:user_name])
       @user_ids = []
        unless @user_data.nil?
          @user_ids << @user_data.id
        end
      end
      render :update do |page|
        page.replace_html "information", :partial => "alphena_user_audit"
      end
    else
      @inspect_control = true
      @user_data = Version.find_all_by_created_at_and_item_type_and_event(Date.today.beginning_of_day..Date.today.end_of_day,"User","update")
      @user_ids = @user_data.map{|p| p.item_id}.uniq
    end
     rescue Exception => e
      Rails.logger.info "Exception in alphena audits controller, alphena user audit action"
      Rails.logger.info e
      flash[:notice] = "Sorry, something went wrong. Please inform administration"
      redirect_to :controller => :alphena_audits, :action => :alphena_audit_links
    end
  end

  def update_user_search
     flash[:clear]
    begin
    @batches = []
    if params[:search_user] == "employee"
      @departments = EmployeeDepartment.active
    elsif params[:search_user] == "student"
      @course = Course.active
    elsif params[:search_user] == "user_name"
      @search_box= "user_name"
    end
    render :update do |page|
      page.replace_html 'user_search', :partial => 'user_search'
    end
    rescue Exception => e
      Rails.logger.info "Exception in alphena audits controller, update user search action"
      Rails.logger.info e
      flash[:notice] = "Sorry, something went wrong. Please inform administration"
      redirect_to :controller => :alphena_audits, :action => :alphena_audit_links
    end
  end

  def update_batch
     flash[:clear]
    begin
    @batches = Batch.find_all_by_course_id(params[:course_id])
    render :update do |page|
      page.replace_html 'alphena_batchs', :partial => 'user_batches', :object => @batches
    end
     rescue Exception => e
      Rails.logger.info "Exception in alphena audits controller,update batch action"
      Rails.logger.info e
      flash[:notice] = "Sorry, something went wrong. Please inform administration"
      redirect_to :controller => :alphena_audits, :action => :alphena_audit_links
    end
  end
end
