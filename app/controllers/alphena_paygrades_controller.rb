class  AlphenaPaygradesController < ApplicationController
 filter_access_to :all
  def add_paygrade
     flash[:clear]
     begin
    @grades =AlphenaPaygrade.find(:all,:order => "name asc")
    @grade = AlphenaPaygrade.new(params[:grade])
    if request.post? and @grade.save
      flash[:notice] =  t('flash9')
      redirect_to :action => "add_paygrade"
    end
     rescue Exception => e
      Rails.logger.info "Exception in alphena paygrades controller, add paygrade action"
      Rails.logger.info e
      flash[:notice] = "Sorry, something went wrong. Please inform administration"
      redirect_to :controller => :reports, :action => :employee_payroll_reports
    end
  end

  def edit_paygrade
     flash[:clear]
     begin
    @grades =AlphenaPaygrade.find(:all, :order=> "name ASC")
    @grade = AlphenaPaygrade.find(params[:id])
    if request.post? and @grade.update_attributes(params[:grade])
      flash[:notice] = "#{t('flash2')}"
      redirect_to :action => "add_paygrade"
    end
    rescue Exception => e
      Rails.logger.info "Exception in alphena paygrades controller, edit paygrade action"
      Rails.logger.info e
      flash[:notice] = "Sorry, something went wrong. Please inform administration"
      redirect_to :controller => :reports, :action => :employee_payroll_reports
    end
end

   def delete_paygrade
     flash[:clear]
    begin
      @grades = AlphenaPaygrade.find(:all, :order=>"name Asc")
       @grade = AlphenaPaygrade.find(params[:id])
         @grade.destroy
       flash[:notice]="#{t('flash3')}"
        redirect_to :action => "add_paygrade"
         rescue Exception => e
      Rails.logger.info "Exception in alphena paygrades controller, delete paygrade action"
      Rails.logger.info e
      flash[:notice] = "Sorry, something went wrong. Please inform administration"
      redirect_to :controller => :reports, :action => :employee_payroll_reports
    end
  end

  def inactivate_paygrade
    if grade = AlphenaPaygrade.update(params[:id], :status => false)
      flash[:notice1]="#{t('flash5')}"
      @grades =AlphenaPaygrade.find(:all,:order => "name asc")
      render :partial => "paygrade"
    end
  end

  def activate_paygrade
    if grade = AlphenaPaygrade.update(params[:id], :status => true)
      flash[:notice1]="#{t('flash6')}"
      @grades =AlphenaPaygrade.find(:all,:order => "name asc")
      render :partial => "paygrade"
    end
  end
end


