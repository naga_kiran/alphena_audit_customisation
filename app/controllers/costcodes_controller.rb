class CostcodesController < ApplicationController
  filter_access_to :all
  def new
    flash[:clear]
    begin
      assigned_category = []
      @costcodes= Costcode.all
      unless @costcodes.empty?
        @costcodes.each do |c|
          assigned_category << c.payroll_category
        end
      end
      @costcode=Costcode.new(params[:costcode])
      @payroll_category = PayrollCategory.all - assigned_category
      if request.post?
        @costcode.save
        flash[:notice] =  t('flash9')
        redirect_to :action => "new"
      end
    rescue Exception => e
      Rails.logger.info "Exception in Cost Code controller, new action"
      Rails.logger.info e
      flash[:notice] = "Sorry, something went wrong. Please inform administration"
      redirect_to :controller => :reports, :action => :employee_payroll_reports

    end
  end

  def edit
    flash[:clear]
    begin
      @costcode=Costcode.find(params[:id])
      assigned_category = []
      @costcodes= Costcode.all
      unless @costcodes.empty?
        @costcodes.each do |c|
          if c != @costcode
            assigned_category << c.payroll_category
          end
        end
      end
      @payroll_category = PayrollCategory.all - assigned_category  
      if request.post?
        flash[:notice] = "#{t('flash2')}"
        @costcode.update_attributes(params[:costcode])
        redirect_to :action => "new"
      end
    rescue Exception => e
      Rails.logger.info "Exception in Cost Code controller, edit action"
      Rails.logger.info e
      flash[:notice] = "Sorry, something went wrong. Please inform administration"
      redirect_to :controller => :reports, :action => :employee_payroll_reports
    end
  end

  def destroy
    flash[:clear]
    begin
      @costcode=Costcode.find(params[:id])
      if @costcode.destroy
        flash[:notice]="#{t('flash3')}"
        redirect_to :action => "new"
      else
        flash[:warn_notice]="Costcode can not be destroy"
      end
    rescue Exception => e
      Rails.logger.info "Exception in Cost Code controller, destroy action"
      Rails.logger.info e
      flash[:notice] = "Sorry, something went wrong. Please inform administration"
      redirect_to :controller => :reports, :action => :employee_payroll_reports
    end
  end
  
end
